module derivative
contains
  subroutine derivative_centered(f,df,coords)
    double precision, dimension(:), intent(in) :: f, coords
    double precision, dimension(:), allocatable, intent(out) :: df
    integer :: i, sz

    sz = size(f)
    if(allocated(df)) then
      deallocate(df)
    end if
    allocate(df(sz))

    do i=2,sz-1
      df(i) = (f(i+1)-f(i-1))/(coords(i+1)-coords(i-1))
    end do
    df(1) = df(2)
    df(sz) = df(sz-1)
  end subroutine
end module

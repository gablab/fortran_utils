# fortran_utils

# modHistogram.f90
class for histograms 
(Gabriele Labanca 2019)

SAMPLE USE:
```
use modHistogram
type(Histogram) :: h
call h%init(nx=10,xmax=dble(3.14),xmin=dble(0.0),&
    ny=2,ymax=dble(1),ymin=dble(0.0))
call h%fill(x=dble(1.3),y=dble(0.2))
call h%prnt("myfile.dat")
```


# modFiniteDifference.f90
```gfortran -L/usr/lib/ -llapack modHistogram.f90 modMatrices.f90 modStats.f90 Lele1991.f90 modFiniteDifferences.f90 test/test_Lele1991.f90```

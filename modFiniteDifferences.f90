module modFiniteDifferences
  use FD_method
  use modMatrices
  implicit none

  double precision, dimension(:,:), allocatable :: A,B,A_inv
  double precision, dimension(:), allocatable :: phi
  double precision, dimension(6) :: bound
  double precision, dimension(:,:), allocatable :: P,Q,M
  double precision, dimension(:), allocatable :: R,K
  double precision, dimension(:), allocatable :: dx


contains

  subroutine init_finite_differences_helmholtz(N,h_in,p1,q1,r1,p2,q2,r2)
    integer, intent(in) :: N
    double precision, dimension(:) :: h_in
    double precision, intent(in) :: p1,q1,r1,p2,q2,r2
    character(200) :: errstr
    character(40) :: err1,err2

    bound(1) = p1
    bound(2) = q1
    bound(3) = r1
    bound(4) = p2
    bound(5) = q2
    bound(6) = r2

    if(size(h_in) .ne. N) then
      write(err1,*) N
      write(err2,*) size(h_in)
      errstr = "modFiniteDifferences : init_finite_differences : mismatch between &
        N ("//trim(err1)//") and size of h_in ("//(err2)//")"
      stop trim(errstr)
    end if

    call init_finite_differences_method(A,B,N,h_in) 
    call print_matrix(A,"log_A.dat")
    call print_matrix(B,"log_B.dat")
    call matr_invert(A,A_inv)
    call print_matrix(A_inv,"log_A_inv.dat")
  
    if(allocated(P)) deallocate(P)
    allocate(P(N,N))
    if(allocated(Q)) deallocate(Q)
    allocate(Q(N,N))
    if(allocated(R)) deallocate(R)
    allocate(R(N))
    if(allocated(M)) deallocate(M)
    allocate(M(N,N))
    if(allocated(K)) deallocate(K)
    allocate(K(N))

    if(allocated(dx)) deallocate(dx)
    allocate(dx(N))
    dx = h_in
  end subroutine

  !subroutine impose_boundaries_dirichlet()
  !!  integer i,j
  !  do i=1,size(bound_phi)
  !    !if(bound_phi(i) .ne. 0) then
  !    if(i .eq. 1 .or. i .eq. size(bound_phi)) then
  !      M(i,:) = 0.0d0
  !      M(i,i) = 1.0d0
  !      K(i) = bound_phi(i)
  !    end if
  !  end do
  !  call print_matrix(M,"log_syst_M.dat")
  !end subroutine

  subroutine impose_boundaries_robin()
    integer :: N
    call print_matrix(M,"log_syst_M_pre.dat")
    N = size(M,1)
    M(1,:)   =  0.0d0
    M(1,1)   =  (bound(1)-bound(2)/dx(1))
    M(1,2)   =  bound(2)/dx(2)
    K(1)     =  bound(3)
    M(N,:)   =  0.0d0
    M(N,N)   =  (bound(4)+bound(5)/dx(1))
    M(N,N-1) = -bound(5)/dx(2)
    K(N)     =  bound(6)
    call print_matrix(M,"log_syst_M_post.dat")
  end subroutine

  subroutine step_helmholtz()
    !double precision, dimension(:,:), allocatable :: M
    !double precision, dimension(:), allocatable :: K

    M = matmul(matmul(P,A_inv),B) + Q
    K = R
    call impose_boundaries_robin()
    call system_solve(M,K,phi)
  end subroutine

  !subroutine step_FTCS(dt,D)
  !  double precision :: dt,D
  !  integer :: i
  !  do i=2,size(phi)-1
  !    if(2*dt*D .gt. dx**2) write(*,*) "COURANT CONDITION VIOLATED"
  !    phi(i) = R(i) + dt*D/dx**2*(R(i+1) -2*R(i) + R(i-1))
  !  end do
  !end subroutine

end module




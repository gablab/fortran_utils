! See Gamet et al., 1999 10.1002/(SICI)1097-0363(19990130)29:2<159::AID-FLD781>3.0.CO;2-9 
module FD_method
  implicit none
  double precision, parameter :: PI_MATH = 4.D0*DATAN(1.D0)
  double precision :: alpha1,alpha2,beta2
  double precision, parameter :: alpha_i = 2.0d0/11.0d0
  double precision, parameter ::  beta_i = 2.0d0/11.0d0

contains

  ! for boundaries formulation: not needed for highest precision
  subroutine set_gamet_boundaries(a1,a2,b2) ! to match Lele: 11,1/10,1/10
    double precision, intent(in) :: a1,a2,b2
    alpha1 = a1
    alpha2 = a2
    beta2 = b2
  end subroutine set_gamet_boundaries

  subroutine init_finite_differences_method(A,B,N,h)
    double precision, dimension(:,:), allocatable :: A,B
    integer, intent(in) :: N
    integer :: i
    double precision, intent(in), dimension(:) :: h
    double precision :: h1,h2,h3,h4, hm,hp,hmm,hpp,h0

    call set_gamet_boundaries(dble(11),dble(1./10),dble(1./10))
    call init_finite_differences_method_loworder(A,B,N,h)
  end subroutine init_finite_differences_method

  subroutine init_finite_differences_method_loworder(A,B,N,h)
    double precision, dimension(:,:), allocatable :: A,B
    integer, intent(in) :: N
    integer :: i
    double precision, intent(in), dimension(:) :: h
    double precision :: h1,h2,h3,h4, hm,hp,hmm,hpp,h0
    if(allocated(A)) deallocate(A)
    allocate(A(N,N))
    if(allocated(B)) deallocate(B)
    allocate(B(N,N))
    A = 0.0d0
    B = 0.0d0

    h1 = h(1)
    h2 = h(2)
    h3 = h(3)
    h4 = h(4)

    A(1,1) = 1.0d0
    A(1,2) = alpha1
    B(1,1) = ( 2*(3*h2+2*h3+h4+2*h3*alpha1+h4*alpha1) )/( h2*(h2+h3)*(h2+h3+h4) )
    B(1,2) = -(2*(2*h2+2*h3+h4-h2*alpha1+2*h3*alpha1+h4*alpha1))/(h2*h3*(h3+h4))
    B(1,3) = (2*(2*h2+h3+h4-h2*alpha1+h3*alpha1+h4*alpha1))/(h3*(h2+h3)*h4)
    B(1,4) = -(2*(2*h2+h3-h2*alpha1+h3*alpha1))/(h4*(h3+h4)*(h2+h3+h4))

    A(2,1) = alpha2 
    A(2,2) = 1.0d0
    A(2,3) = beta2
    B(2,1) = (2*(2*h3+h4+3*h2*alpha2+2*h3*alpha2+h4*alpha2-h3*beta2+h4*beta2))/(h2*(h2+h3)*(h2+h3+h4))
    B(2,2) = -(2*(2*h3-h2+h4+2*h2*alpha2+2*h3*alpha2+h4*alpha2-h2*beta2-h3*beta2+h4*beta2))/(h2*h3*(h3+h4))
    B(2,3) = (2*(-h2+h3+h4+2*h2*alpha2+h3*alpha2+h4*alpha2-h2*beta2-2*h3*beta2+h4*beta2))/(h3*(h2+h3)*h4)
    B(2,4) = (2*(h2-h3-2*h2*alpha2-h3*alpha2+h2*beta2+2*h3*beta2))/(h4*(h3+h4)*(h2+h3+h4))

    do i=3,N-2
      hmm = h(i-2)
      hm = h(i-1)
      h0 = h(i)
      hp = h(i+1)
      hpp = h(i+2)
      A(i,i-1) = alpha_i 
      A(i,i) = 1.0d0
      A(i,i+1) = beta_i 

      B(i,i-2) = (2*(-2*h0*hp + hp**2 - h0*hpp + hp*hpp + 3*h0**2*alpha_i + 4*h0*hp*alpha_i + hp**2*alpha_i&
        +2*h0*hpp*alpha_i + hp*hpp*alpha_i + h0*hp*beta_i + hp**2*beta_i - h0*hpp*beta_i - 2*hp*hpp*beta_i))&
        /(hm*(hm+h0)*(hm+h0+hp)*(hm+h0+hp+hpp))
      B(i,i-1) = (2* (2*hm*hp + 2*h0*hp - hp**2 + hm*hpp + h0*hpp - hp*hpp + 2*hm*hp*alpha_i &
        - 3*h0**2*alpha_i + 3*hm*h0*alpha_i - 4*h0*hp*alpha_i - hp**2*alpha_i + hm*hpp*alpha_i - 2*h0*hpp*alpha_i - h0*hp*beta_i&
        - hm*hp*beta_i - hp*hpp*alpha_i - hp**2*beta_i + hm*hpp*beta_i + h0*hpp*beta_i + 2*hp*hpp*beta_i))&
        /(hm*h0*(h0+hp)*(h0+hp+hpp))
      B(i,i+1) = (2*(-hm*h0-h0**2+hm*hp+2*h0*hp+hm*hpp+2*h0*hpp+2*hm*h0*alpha_i-h0**2*alpha_i&
        +hm*hp*alpha_i - h0*hp*alpha_i + hm*hpp*alpha_i - h0*hpp*alpha_i - hm*h0*beta_i - h0**2*beta_i &
        -2*hm*hp*beta_i - 4*h0*hp*beta_i - 3*hp**2*beta_i + hm*hpp*beta_i + 2*h0*hpp*beta_i + 3*hp*hpp*beta_i))&
        /(hp*hpp*(h0+hp)*(hm+h0+hp))
      B(i,i+2) = (2*(hm*h0+h0**2-hm*hp-2*h0*hp - 2*hm*h0*alpha_i + h0**2*alpha_i - hm*hp*alpha_i &
        +h0*hp*alpha_i + hm*h0*beta_i + h0**2*beta_i + 2*hm*hp*beta_i + 4*h0*hp*beta_i + 3*hp**2*beta_i))&
        /(hpp*(hp+hpp)*(h0+hp+hpp)*(hm+h0+hp+hpp))
      B(i,i) = -(B(i,i-2) + B(i,i-1) + B(i,i+1) + B(i,i+2))
    end do

    h1 = h(N)
    h2 = h(N-1)
    h3 = h(N-2)
    h4 = h(N-3)

    A(N,N) = 1.0d0
    A(N,N-1) = alpha1
    B(N,N) = ( 2*(3*h2+2*h3+h4+2*h3*alpha1+h4*alpha1) )/( h2*(h2+h3)*(h2+h3+h4) )
    B(N,N-1) = -(2*(2*h2+2*h3+h4-h2*alpha1+2*h3*alpha1+h4*alpha1))/(h2*h3*(h3+h4))
    B(N,N-2) = (2*(2*h2+h3+h4-h2*alpha1+h3*alpha1+h4*alpha1))/(h3*(h2+h3)*h4)
    B(N,N-3) = -(2*(2*h2+h3-h2*alpha1+h3*alpha1))/(h4*(h3+h4)*(h2+h3+h4))

    A(N-1,N) = alpha2 
    A(N-1,N-1) = 1.0d0
    A(N-1,N-2) = beta2
    B(N-1,N) = (2*(2*h3+h4+3*h2*alpha2+2*h3*alpha2+h4*alpha2-h3*beta2+h4*beta2))/(h2*(h2+h3)*(h2+h3+h4))
    B(N-1,N-1) = -(2*(2*h3-h2+h4+2*h2*alpha2+2*h3*alpha2+h4*alpha2-h2*beta2-h3*beta2+h4*beta2))/(h2*h3*(h3+h4))
    B(N-1,N-2) = (2*(-h2+h3+h4+2*h2*alpha2+h3*alpha2+h4*alpha2-h2*beta2-2*h3*beta2+h4*beta2))/(h3*(h2+h3)*h4)
    B(N-1,N-3) = (2*(h2-h3-2*h2*alpha2-h3*alpha2+h2*beta2+2*h3*beta2))/(h4*(h3+h4)*(h2+h3+h4))


    !print*,'__',B(1,:)
    !print*,'__',B(2,:)
    !print*,'__',B(3,:)
    !print*,'__',B(N-2,:)
    !print*,'__',B(N-1,:)
    !print*,'__',B(N,:)

  end subroutine init_finite_differences_method_loworder


  subroutine init_finite_differences_method_highorder(A,B,N,h)
    double precision, dimension(:,:), allocatable :: A,B
    integer, intent(in) :: N
    integer :: i
    double precision, intent(in), dimension(:) :: h
    double precision :: h1,h2,h3,h4, hm,hp,hmm,hpp,h0
    if(allocated(A)) deallocate(A)
    allocate(A(N,N))
    if(allocated(B)) deallocate(B)
    allocate(B(N,N))
    A = 0.0d0
    B = 0.0d0

    h1 = h(1)
    h2 = h(2)
    h3 = h(3)
    h4 = h(4)

    A(1,1) = 1.0d0
    A(1,2) = (3*h2**2 + 4*h2*h3 + 2*h2*h4 + h3**2 + h3*h4)/(2*h2*h3 + h2*h4 - h3**2 - h3*h4)
    B(1,1) = 6*(4*h2*h3 + 2*h2*h4 + 3*h3**2 + 3*h3*h4 + h4**2)/(2*h2**3*h3 + h2**3*h4 + &
      3*h2**2*h3**2 + 3*h2**2*h3*h4 + h2**2*h4**2 - h3**4 - 2*h3**3*h4 - h3**2*h4**2)
    B(1,2) = 6*(h2**2 - 2*h2*h3 - h2*h4 - 3*h3**2 - 3*h3*h4 - h4**2)/(h3*(2*h2*h3**2 + &
      3*h2*h3*h4 + h2*h4**2 - h3**3 - 2*h3**2*h4 - h3*h4**2))
    B(1,3) = 6*h2*(-h2**2 + h2*h3 + h2*h4 + h3**2 + 2*h3*h4 + h4**2)/(h3*h4*(2*h2**2*h3 + &
      h2**2*h4 + h2*h3**2 - h3**3 - h3**2*h4))
    B(1,4) = 6*h2*(h2**2 - h2*h3 - h3**2)/(h4*(2*h2**2*h3**2 + 3*h2**2*h3*h4 + h2**2*h4**2 +&
      h2*h3**3 + 3*h2*h3**2*h4 + 3*h2*h3*h4**2 + h2*h4**3 - h3**4 - 3*h3**3*h4 - 3*h3**2*h4**2 - h3*h4**3))

    A(2,1) = (56*h2**4*h3 + 28*h2**4*h4 + 80*h2**3*h3**2 + 44*h2**3*h3*h4 + 12*h2**3*h4**2 - &
      9*h2**2*h3**3 - 15*h2**2*h3**2*h4 + 9*h2**2*h3*h4**2 - 38*h2*h3**4 - 49*h2*h3**3*h4 - &
      15*h2*h3**2*h4**2 - 5*h3**5 - 14*h3**4*h4 - 9*h3**3*h4**2)/(36*h2**5 + 128*h2**4*h3 + &
      52*h2**4*h4 + 173*h2**3*h3**2 + 155*h2**3*h3*h4 + 21*h2**3*h4**2 + 111*h2**2*h3**3 + &
      168*h2**2*h3**2*h4 + 48*h2**2*h3*h4**2 + 35*h2*h3**4 + 79*h2*h3**3*h4 + 36*h2*h3**2*h4**2 + &
      5*h3**5 + 14*h3**4*h4 + 9*h3**3*h4**2)
    A(2,2) = 1.0d0
    A(2,3) = h2*(8*h2**3*h3 + 4*h2**3*h4 + 5*h2**2*h3**2 + 5*h2**2*h3*h4 + 3*h2**2*h4**2 - &
      6*h2*h3**3 - 9*h2*h3**2*h4 - 3*h2*h3*h4**2 - 3*h3**4 - 6*h3**3*h4 - 3*h3**2*h4**2)/(36*h2**5 + &
      128*h2**4*h3 + 52*h2**4*h4 + 173*h2**3*h3**2 + 155*h2**3*h3*h4 + 21*h2**3*h4**2 + &
      111*h2**2*h3**3 + 168*h2**2*h3**2*h4 + 48*h2**2*h3*h4**2 + 35*h2*h3**4 + 79*h2*h3**3*h4 + &
      36*h2*h3**2*h4**2 + 5*h3**5 + 14*h3**4*h4 + 9*h3**3*h4**2)
    B(2,1) = 12*(40*h2**3*h3 + 20*h2**3*h4 + 60*h2**2*h3**2 + 60*h2**2*h3*h4 + 20*h2**2*h4**2 + &
      19*h2*h3**3 + 41*h2*h3**2*h4 + 29*h2*h3*h4**2 + 6*h2*h4**3 - 3*h3**4 + 3*h3**3*h4 + &
      7*h3**2*h4**2 + 3*h3*h4**3)/(36*h2**6 + 164*h2**5*h3 + 88*h2**5*h4 + 301*h2**4*h3**2 + &
      335*h2**4*h3*h4 + 73*h2**4*h4**2 + 284*h2**3*h3**3 + 496*h2**3*h3**2*h4 + 224*h2**3*h3*h4**2 &
      + 21*h2**3*h4**3 + 146*h2**2*h3**4 + 358*h2**2*h3**3*h4 + 252*h2**2*h3**2*h4**2 &
      + 48*h2**2*h3*h4**3 + 40*h2*h3**5 + 128*h2*h3**4*h4 + 124*h2*h3**3*h4**2 + 36*h2*h3**2*h4**3 &
      + 5*h3**6 + 19*h3**5*h4 + 23*h3**4*h4**2 + 9*h3**3*h4**3)
    B(2,2) = 12*(6*h2**4 - 14*h2**3*h3 - 6*h2**3*h4 - 43*h2**2*h3**2 - 40*h2**2*h3*h4 - 14*h2**2*h4**2 &
      - 20*h2*h3**3 - 37*h2*h3**2*h4 - 26*h2*h3*h4**2 - 6*h2*h4**3 + 3*h3**4 - 3*h3**3*h4 - &
      7*h3**2*h4**2 - 3*h3*h4**3)/(h3*(36*h2**4*h3 + 36*h2**4*h4 + 92*h2**3*h3**2 + 144*h2**3*h3*h4 + &
      52*h2**3*h4**2 + 81*h2**2*h3**3 + 184*h2**2*h3**2*h4 + 124*h2**2*h3*h4**2 + 21*h2**2*h4**3 + &
      30*h2*h3**4 + 95*h2*h3**3*h4 + 92*h2*h3**2*h4**2 + 27*h2*h3*h4**3 + 5*h3**5 + 19*h3**4*h4 + &
      23*h3**3*h4**2 + 9*h3**2*h4**3))
    B(2,3) = 12*h2*(-6*h2**4 + 8*h2**3*h3 + 6*h2**3*h4 + 17*h2**2*h3**2 + 26*h2**2*h3*h4 + &
      14*h2**2*h4**2 + 3*h2*h3**3 + 17*h2*h3**2*h4 + 20*h2*h3*h4**2 + 6*h2*h4**3 - 2*h3**4 - &
      h3**3*h4 + 4*h3**2*h4**2 + 3*h3*h4**3)/(h3*h4*(36*h2**5 + 128*h2**4*h3 + 52*h2**4*h4 + &
      173*h2**3*h3**2 + 155*h2**3*h3*h4 + 21*h2**3*h4**2 + 111*h2**2*h3**3 + 168*h2**2*h3**2*h4 + &
      48*h2**2*h3*h4**2 + 35*h2*h3**4 + 79*h2*h3**3*h4 + 36*h2*h3**2*h4**2 + 5*h3**5 + 14*h3**4*h4 + &
      9*h3**3*h4**2))
    B(2,4) = 12*h2*(6*h2**4 - 8*h2**3*h3 - 17*h2**2*h3**2 - 3*h2*h3**3 + 2*h3**4)/(h4*(36*h2**5*h3 &
      + 36*h2**5*h4 + 128*h2**4*h3**2 + 216*h2**4*h3*h4 + 88*h2**4*h4**2 + 173*h2**3*h3**3 + &
      420*h2**3*h3**2*h4 + 320*h2**3*h3*h4**2 + 73*h2**3*h4**3 + 111*h2**2*h3**4 + &
      360*h2**2*h3**3*h4 + 400*h2**2*h3**2*h4**2 + 172*h2**2*h3*h4**3 + 21*h2**2*h4**4 + &
      35*h2*h3**5 + 144*h2*h3**4*h4 + 210*h2*h3**3*h4**2 + 128*h2*h3**2*h4**3 + 27*h2*h3*h4**4 &
      + 5*h3**6 + 24*h3**5*h4 + 42*h3**4*h4**2 + 32*h3**3*h4**3 + 9*h3**2*h4**4))

    do i=3,N-2
      hmm = h(i-2)
      hm = h(i-1)
      h0 = h(i)
      hp = h(i+1)
      hpp = h(i+2)
      A(i,i-1) = alpha_i 
      A(i,i) = 1.0d0
      A(i,i+1) = beta_i 

      B(i,i-2) = (2*(-2*h0*hp + hp**2 - h0*hpp + hp*hpp + 3*h0**2*alpha_i + 4*h0*hp*alpha_i + hpp**2*alpha_i&
        +2*h0*hpp*alpha_i + hp*hpp*alpha_i + h0*hp*beta_i + hpp**2*beta_i - h0*hpp*beta_i - 2*hp*hpp*beta_i))&
        /(hm*(hm+h0)*(hm+h0+hp)*(hm+h0+hp+hpp))
      B(i,i-1) = (2*(2*hm*hp+2*h0*hp-hp**2+hm*hpp+h0*hpp-hp*hpp+2*hm*hp*alpha_i &
        -3*h0**2*alpha_i + 3*hm*h0*alpha_i - 4*h0*hp*alpha_i - hp**2*alpha_i + hm*hpp*alpha_i - 2*h0*hpp*alpha_i - h0*hp*beta_i&
        - hm*hp*beta_i - hp*hpp*alpha_i - hp**2*beta_i + hm*hpp*beta_i + h0*hpp*beta_i + 2*hp*hpp*beta_i))&
        /(hm*h0*(h0+hp)*(h0+hp+hpp))
      B(i,i+1) = (2*(-hm*h0-h0**2+hm*hp+2*h0*hp+hm*hpp+2*h0*hpp+2*hm*h0*alpha_i-h0**2*alpha_i&
        +hm*hp*alpha_i - h0*hp*alpha_i + hm*hpp*alpha_i - h0*hpp*alpha_i - hm*h0*beta_i - h0**2*beta_i &
        -2*hm*hp*beta_i - 4*h0*hp*beta_i - 3*hp**2*beta_i + hm*hpp*beta_i + 2*h0*hpp*beta_i + 3*hp*hpp*beta_i))&
        /(hp*hpp*(h0+hp)*(hm+h0+hp))
      B(i,i+2) = (2*(hm*h0+h0**2-hm*hp-2*h0*hp - 2*hm*h0*alpha_i + h0**2*alpha_i - hm*hp*alpha_i &
        +h0*hp*alpha_i + hm*h0*beta_i + h0**2*beta_i + 2*hm*hp*beta_i + 4*h0*hp*beta_i + 3*hp**2*beta_i))&
        /(hpp*(hp+hpp)*(h0+hp+hpp)*(hm+h0+hp+hpp))
      B(i,i) = -(B(i,i-2) + B(i,i-1) + B(i,i+1) + B(i,i+2))
    end do

    h1 = h(N)
    h2 = h(N-1)
    h3 = h(N-2)
    h4 = h(N-3)

    A(N,N) = A(1,1)
    A(N,N-1) = A(1,2)
    B(N,N) = B(1,1)
    B(N,N-1) = B(1,2)
    B(N,N-2) = B(1,3)
    B(N,N-3) = B(1,4)

    A(N-1,N) = A(2,1)
    A(N-1,N-1) = 1.0d0
    A(N-1,N-2) = A(2,3)
    B(N-1,N) = B(2,1)
    B(N-1,N-1) = B(2,2)
    B(N-1,N-2) = B(2,3)
    B(N-1,N-3) = B(2,4)


    !print*,'__',B(1,:)
    !print*,'__',B(2,:)
    !print*,'__',B(3,:)
    !print*,'__',B(N-2,:)
    !print*,'__',B(N-1,:)
    !print*,'__',B(N,:)

  end subroutine init_finite_differences_method_highorder
  

  subroutine init_finite_differences_method_periodic(A,B,N,h)
    double precision, dimension(:,:), allocatable :: A,B
    integer, intent(in) :: N
    integer :: i
    double precision, intent(in), dimension(:) :: h
    double precision :: h1,h2,h3,h4, hm,hp,hmm,hpp,h0
    if(allocated(A)) deallocate(A)
    allocate(A(N,N))
    if(allocated(B)) deallocate(B)
    allocate(B(N,N))
    A = 0.0d0
    B = 0.0d0

    do i=1,N
      hmm = h(periodize(i-2,N))
      hm  = h(periodize(i-1,N))
      h0  = h(periodize(i,  N))
      hp  = h(periodize(i+1,N))
      hpp = h(periodize(i+2,N))
      A(periodize(i,N),periodize(i-1,N)) = alpha_i 
      A(periodize(i,N),periodize(i,  N))   = 1.0d0
      A(periodize(i,N),periodize(i+1,N)) = beta_i 

      B(periodize(i,N),periodize(i-2,N)) = (2*(-2*h0*hp + hp**2 - h0*hpp + hp*hpp + 3*h0**2*alpha_i + &
        4*h0*hp*alpha_i + hpp**2*alpha_i&
        +2*h0*hpp*alpha_i + hp*hpp*alpha_i + h0*hp*beta_i + hpp**2*beta_i - h0*hpp*beta_i - 2*hp*hpp*beta_i))&
        /(hm*(hm+h0)*(hm+h0+hp)*(hm+h0+hp+hpp))
      B(periodize(i,N),periodize(i-1,N)) = (2*(2*hm*hp+2*h0*hp-hp**2+hm*hpp+h0*hpp-hp*hpp+2*hm*hp*alpha_i &
        -3*h0**2*alpha_i + 3*hm*h0*alpha_i - 4*h0*hp*alpha_i - hp**2*alpha_i + hm*hpp*alpha_i - &
        2*h0*hpp*alpha_i - h0*hp*beta_i&
        - hm*hp*beta_i - hp*hpp*alpha_i - hp**2*beta_i + hm*hpp*beta_i + h0*hpp*beta_i + 2*hp*hpp*beta_i))&
        /(hm*h0*(h0+hp)*(h0+hp+hpp))
      B(periodize(i,N),periodize(i+1,N)) = (2*(-hm*h0-h0**2+hm*hp+2*h0*hp+hm*hpp+2*h0*hpp+&
        2*hm*h0*alpha_i-h0**2*alpha_i&
        +hm*hp*alpha_i - h0*hp*alpha_i + hm*hpp*alpha_i - h0*hpp*alpha_i - hm*h0*beta_i - h0**2*beta_i &
        -2*hm*hp*beta_i - 4*h0*hp*beta_i - 3*hp**2*beta_i + hm*hpp*beta_i + 2*h0*hpp*beta_i + 3*hp*hpp*beta_i))&
        /(hp*hpp*(h0+hp)*(hm+h0+hp))
      B(periodize(i,N),periodize(i+2,N)) = (2*(hm*h0+h0**2-hm*hp-2*h0*hp - &
        2*hm*h0*alpha_i + h0**2*alpha_i - hm*hp*alpha_i &
        +h0*hp*alpha_i + hm*h0*beta_i + h0**2*beta_i + 2*hm*hp*beta_i + 4*h0*hp*beta_i + 3*hp**2*beta_i))&
        /(hpp*(hp+hpp)*(h0+hp+hpp)*(hm+h0+hp+hpp))
      B(periodize(i,N),periodize(i,N)) = -(B(periodize(i,N),periodize(i-2,N)) + &
        B(periodize(i,N),periodize(i-1,N)) + &
        B(periodize(i,N),periodize(i+1,N)) + B(periodize(i,N),periodize(i+2,N)))
    end do

  end subroutine init_finite_differences_method_periodic


  subroutine make_grid(grid,a,b,alpha,N)
    double precision, dimension(:,:), allocatable :: grid
    integer, intent(in) :: N
    integer :: ng,ig
    double precision :: randcoeff = 0.3 ! cfr. Gamet1999
    double precision :: a,b,alpha
    ng = N 
    if(allocated(grid)) deallocate(grid)
    allocate(grid(N,2))
    call srand(78936)
    do ig=1,ng
      ! uniform
      grid(ig,1) = a + (b-a)*dble(ig-1)/(ng-1)
      grid(ig,2) = (b-a)/ng
      ! fake uniform
      !grid(ig,1) = a + (b-a)*(dble(ig)/ng + (dble(ig)/ng)**2 )
      !grid(ig,2) = (b-a)*(1/dble(ng) + dble(2*ig+1)/(dble(ng)**2))
      ! Chebyshev-Gauss-Lobatto
      !grid(ig,1) = (b+a)/2. + (b-a)*cos(PI_MATH*(2.*ig-1.)/(2.*ng))/2.
      !grid(ig,2) = (b-a)*( cos((2*ig+1)/(2.*ng)*PI_MATH) - cos((2*ig-1)/(2.*ng)*PI_MATH) )
      ! Shukla
      !grid(ig,1) = (b+a)/2. + (b-a)*asin(-alpha*cos(PI_MATH*ig/ng))/(2.*asin(alpha))
      !grid(ig,2) = (b-a)*( asin(-alpha*cos(PI_MATH*ig/ng))/(2.*asin(alpha)) &
      !  - asin(-alpha*cos(PI_MATH*(ig-1)/ng))/(2.*asin(alpha)) )
      ! Random
      !grid(ig,1) = a + (b-a)*(dble(ig)-1+randcoeff*rand())/ng
      !if(ig.eq.1) then
      !  grid(ig,2) = (b-a)/dble(ng)
      !else
      !  grid(ig,2) = grid(ig,1) - grid(ig-1,1)
      !endif
    end do
  end subroutine

  function periodize(x,N) result(y)
        integer, intent(in) :: x,N
        integer :: y
        y = mod(x,N)
        if(y.le.0) y = y + N
  end function periodize

end module


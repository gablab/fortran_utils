module FD_method
  implicit none
  double precision, parameter :: PI_MATH = 4.D0*DATAN(1.D0)

contains

  subroutine init_finite_differences_method(A,B,N,h)
    double precision, dimension(:,:), allocatable :: A,B
    integer, intent(in) :: N
    integer :: i
    double precision, intent(in), dimension(:) :: h
    double precision :: h1,h2,h3, t1,t2,t3, hm,hp

    !do i=1,N
    !  write(91,*) i,h(i)
    !end do

    if(allocated(A)) deallocate(A)
    allocate(A(N,N))
    if(allocated(B)) deallocate(B)
    allocate(B(N,N))
    A = 0.0d0
    B = 0.0d0

    h1 = h(1)
    h2 = h(2)
    h3 = h(3)

    A(1,1) = 1.0d0
    A(1,2) = ( h1*( 3*h1 + 4*h2 + 2*h3) + h2*( h2 + h3 ) ) &
      / ( h1*( 2*h2 + h3 ) - ( h2*( h2 + h3 ) ) )
    B(1,1) = (-6*h2**2-(4*h2+2*h3)*(6*h1+3*h2+3*h3)) &
      / ((h1+h2)*(h1+h2+h3)*(h2*(h2+h3)-h1*(2*h2+h3)) )
    B(1,2) = (6*(h1+h2)**2 + 6*(2*h1+2*h2+h3)*(h2+h3-h1)  ) &
      / (h2*(h2+h3)*(h2*(h2+h3)-h1*(2*h2+h3)))
    B(1,3) = (6*h1*((h2+h3)**2+h1*(h2+h3)-h1**2)) &
      / ( h2*h3*(h1+h2)*(h1*(2*h2+h3)-h2*(h2+h3))  )
    B(1,4) = ( 6*h1*(h1*h2+h2**2-h1**2)  ) &
      / ( h2*h3*(h1+h2)*(h1*(2*h2+h3)-h2*(h2+h3) ))

    do i=2,N-1
      hm = h(i)
      hp = h(i+1)
      A(i,i-1) = Aim1(hm,hp)
      A(i,i) = 1.0d0
      A(i,i+1) = Aip1(hm,hp)
      B(i,i-1) = Bim1(hm,hp)
      B(i,i) = Bii(hm,hp)
      B(i,i+1) = Bip1(hm,hp) 
    end do

    !hm = h(N-1)
    !hp = h(N)
    !A(N-1,N-2) = Aim1(hm,hp)
    !A(N-1,N-1) = 1.1d0
    !A(N-1,N) = ( t1*(3*t1+4*t2+2*t3) + t2*(t2+t3) ) &
    !  / ( t1*(2*t2+t3) - t2*(t2+t3) )
    !B(N-1,N-2) = Bim1(hm,hp)
    !B(N-1,N-1) = Bii(hm,hp)
    !B(N-1,N) = Bip1(hm,hp) 

    t1 = -h(N)
    t2 = -h(N-1)
    t3 = -h(N-2)
    A(N,N-1) = ( t1*(3*t1+4*t2+2*t3) + t2*(t2+t3) ) &
      / ( t1*(2*t2+t3) - t2*(t2+t3) )
    A(N,N) = 1.0d0
    B(N,N-3) = (6*t1*(t1*t2+t2**2-t1**2)) &
      / (t2*t3*(t1+t2)*(t1*(2*t2+t3)-t2*(t2+t3)))
    B(N,N-2) = ( 6*t1*((t2+t3)**2+t1*(t2+t3)-t1**2 )) &
      / (t2*t3*(t1+t2)*(t1*(2*t2+t3)-t2*(t2+t3)))
    B(N,N-1) = (6*(t1+t2)**2+6*(2*t1+2*t2+t3)*(t2+t3-t1)) &
      / (t2*(t2+t3)*(t2*(t2+t3)-t1*(2*t2+t3)))
    B(N,N) = (-6*t2**2-(4*t2+2*t3)*(6*t1+3*t2+3*t3)) &
      / ((t1+t2)*(t1+t2+t3)*(t2*(t2+t3)-t1*(2*t2+t3)))

  end subroutine

  function Aim1(hm,hp) result(y)
    double precision, intent(in) :: hm,hp
    double precision :: y
    y = hp/(hm+hp)*(hm**2+hp*hm-hp**2)/(hm**2+3*hp*hm+hp**2)
  end function
  function Aip1(hm,hp) result(y)
    double precision, intent(in) :: hm,hp
    double precision :: y
    y = hm/(hm+hp)*(hp**2+hp*hm-hm**2)/(hm**2+3*hp*hm+hp**2)
  end function
  function Bim1(hm,hp) result(y)
    double precision, intent(in) :: hm,hp
    double precision :: y
    y = hp/(hm+hp)*(12.0d0)/(hm**2+3*hp*hm+hp**2)
  end function
  function Bii(hm,hp) result(y)
    double precision, intent(in) :: hm,hp
    double precision :: y
    y = (-12.0d0)/(hm**2+3*hp*hm+hp**2)
  end function
  function Bip1(hm,hp) result(y)
    double precision, intent(in) :: hm,hp
    double precision :: y
    y = hm/(hm+hp)*(12.0d0)/(hm**2+3*hp*hm+hp**2)
  end function

  subroutine make_grid(grid,a,b,alpha,N)
    double precision, dimension(:,:), allocatable :: grid
    integer, intent(in) :: N
    integer :: ng,ig
    double precision :: a,b,alpha
    ng = N 
    if(allocated(grid)) deallocate(grid)
    allocate(grid(N,2))
    do ig=1,ng
      ! NOTA BENE: this matrices are not grid-independent! Always use the grid below
      grid(ig,1) = (b+a)/2. + (b-a)*asin(-alpha*cos(PI_MATH*ig/ng))/(2.*asin(alpha))
      grid(ig,2) = (b-a)*( asin(-alpha*cos(PI_MATH*ig/ng))/(2.*asin(alpha)) &
        - asin(-alpha*cos(PI_MATH*(ig-1)/ng))/(2.*asin(alpha)) )
    end do
  end subroutine

end module


module modFiles
contains
  subroutine open_newfile(u,filename)
    integer, intent(in) :: u
    character(len=*), intent(in) :: filename
    integer :: stat

    open(unit=u, iostat=stat, file=trim(filename), status='old')
    if (stat == 0) close(u, status='delete')
    open(unit=u,file=trim(filename),status='new')
  end subroutine
  
  function count_lines(filename) result(nlines) ! https://stackoverflow.com/a/32682634/5599687
    implicit none
    character(len=*)    :: filename
    integer             :: nlines
    integer             :: io
    open(10,file=trim(filename), iostat=io, status='old')
    if (io.ne.0) stop 'Cannot open file! '//filename
    nlines = 0
    do
      read(10,*,iostat=io)
      if (io/=0) exit
      nlines = nlines + 1
    end do
    close(10)
  end function count_lines
end module

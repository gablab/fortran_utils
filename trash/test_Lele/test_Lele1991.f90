program main
  use modFiniteDifferences
  use modStats
  implicit none
  integer, parameter :: N = 50
  integer, parameter :: Ntime = 1000
  integer :: i,time
  double precision, parameter :: dt = 0.1d0, D = 0.05d0
  double precision, dimension(:), allocatable :: d2phi, analytical
  double precision :: xi, phi_0, dphi_0, d2phi_0

  allocate(d2phi(N))
  allocate(analytical(N))
  allocate(phi(N))

  !----------------------------!
  !----- TEST KNOWN CASES -----!
  !----------------------------!
  write(*,*) "Test derivative: sin -> -sin"
  call init_finite_differences_helmholtz(N,h_in=dble(6.28/N),flag_bound=2)
  do i=1,N
    phi(i) = sin(dble(i)/N*6.28)
  end do
  open(66,file='sin.dat')
  write(66,*) "position phi d2phi"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) i*6.28/N,phi(i),d2phi(i)
  end do
  close(66)

  write(*,*) "Test derivative: tan -> 2tan(1+tan^2)"
  call init_finite_differences_helmholtz(N,h_in=dble(2.0/N),flag_bound=2)
  do i=1,N
    phi(i) = tan(-1.0d0+dble(i)*2/N)
  end do
  open(66,file='tan.dat')
  write(66,*) "position phi d2phi"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) -1.0d0+dble(i)*2/N,phi(i),d2phi(i)
  end do
  close(66)

  call init_finite_differences_helmholtz(N,h_in=1.0d0)
  write(*,*) "Test derivative: x^3 -> 6x"
  do i=1,N
    phi(i) = i**3
  end do
  open(66,file='pow.dat')
  write(66,*) "position phi d2phi"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) i,phi(i),d2phi(i)
  end do
  close(66)



  !---------------------------!
  !----- SOLVE DIFFUSION -----!
  !---------------------------!
  write(*,*) "Solving diffusion"

  call init_finite_differences_helmholtz(N,h_in=0.1d0,flag_bound=1)
  ! fill matrices for Helmholtz equation
  P = 0.0d0
  Q = 0.0d0
  do i=1,N
    P(i,i) = -dt*D
    Q(i,i) = 1.0d0
    phi(i) = sin(dble(i)/N*6.28) - cos(dble(i)/N*1.25) + 0.85*sin(dble(i)/N*10.34) + exp(-dble(i)/N) + dble(i)/N
    xi = dble(i-1)/(N-1)
    analytical(i) = xi*0.5
  end do
  bound(1) = analytical(1)
  bound(N) = analytical(N)
  R = analytical(i)

  open(67,file='diffusion.dat')
  write(67,*) "time position phi d2phi error"
  do time=1,Ntime
    d2phi = linapp(A_inv,(linapp(B,phi)))
    do i=1,N
      xi = dble(i-1)/(N-1)
      write(67,*) time,xi,phi(i),d2phi(i),mean_root_difference(phi,analytical)
    end do
    R = phi
    !call step_FTCS(dt,D)
    call step_helmholtz
  end do
  close(67)

  !----------------------------!
  !----- SOLVE HELMHOLTZ  -----!
  !----------------------------!
  write(*,*) "Solving Helmholtz equation"
  call init_finite_differences_helmholtz(N,h_in=0.1d0,flag_bound=0)
  ! fill matrices for Helmholtz equation
  P = 0.0d0
  Q = 0.0d0
  phi = 0.0d0
  R = 0.0d0

  do i=1,N
    P(i,i) = 1.0d0
    Q(i,i) = 1.0d0
    xi = dble(i-1)/(N-1)
    R(i) = -sin(3.14d0*xi) * 1.0d-3
  end do

  call step_helmholtz
  open(66,file='helmholtz.dat')
  write(66,*) "position phi"
  do i=1,N
    xi = dble(i-1)/(N-1)
    write(66,*) xi,phi(i)
  end do
  close(66)

  !-------------------------!
  !----- SOLVE POISSON -----!
  !-------------------------!
  write(*,*) "Solving Poisson equation"
  call init_finite_differences_helmholtz(N,h_in=0.1d0,flag_bound=1)
  ! fill matrices for Helmholtz equation
  P = 0.0d0
  Q = 0.0d0
  phi = 0.0d0
  R = 0.0d0

  phi_0 = 1.0d0
  dphi_0 = 1.0d0
  d2phi_0 = 2.0d0

  do i=1,N
    P(i,i) = 1.0d0
    R(i) = 1.0d0 * d2phi_0
    xi = dble(i-1)/(N-1)
    analytical(i) = phi_0 + dphi_0 * xi + d2phi_0 * xi**2 * 0.5
  end do
  bound(1) = analytical(1)
  bound(N) = analytical(N)


  call step_helmholtz
  open(66,file='poisson.dat')
  write(66,*) "position phi analytical error"
  do i=1,N
    xi = dble(i-1)/(N-1)
    write(66,*) xi,phi(i),analytical(i),mean_root_difference(phi,analytical)
  end do
  !call step_FTCS(dt,D)
  close(66)


contains

end program

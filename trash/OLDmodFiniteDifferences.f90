module modFiniteDifferences
  use FD_method
  use modMatrices
  implicit none

  double precision, dimension(:,:), allocatable :: A,B,A_inv,B_inv
  double precision, dimension(:), allocatable :: bound, phi
  double precision, dimension(:,:), allocatable :: P,Q,M
  double precision, dimension(:), allocatable :: R,K
  double precision :: dx


contains

  ! initialize matrices A, B and boundary array:
  !  - if bound_array argument is not provided, the boundary array will be set to 0
  !  - if flag_bound == 0 or not present, first and last rows will give f''=0
  !  - if flag_bound == 1, first and last rows will implement the "boundary formulation for the second derivative"
  subroutine init_finite_differences(N,h_in,bound_array,flag_bound)
    integer, intent(in) :: N
    double precision, dimension(:), intent(in), optional :: bound_array
    double precision :: h_in
    integer, intent(in), optional :: flag_bound
    if(allocated(bound)) deallocate(bound)
    allocate(bound(N))

    if(present(bound_array)) then
      bound = bound_array
    else
      bound = 0
    end if
    if(present(flag_bound)) then
      call init_finite_differences_method(A,B,N,h_in,flag_bound=flag_bound) 
    else
      call init_finite_differences_method(A,B,N,h_in) 
    end if
    call matr_invert(A,A_inv)

    dx = h_in
  end subroutine

  subroutine init_finite_differences_helmholtz(N,h_in,flag_bound)
    integer, intent(in) :: N
    double precision :: h_in
    integer, intent(in), optional :: flag_bound
    if(present(flag_bound)) then
      call init_finite_differences(N,h_in,flag_bound=flag_bound)
    else
      call init_finite_differences(N,h_in)
    end if
    if(allocated(P)) deallocate(P)
    allocate(P(N,N))
    if(allocated(Q)) deallocate(Q)
    allocate(Q(N,N))
    if(allocated(R)) deallocate(R)
    allocate(R(N))
    if(allocated(M)) deallocate(M)
    allocate(M(N,N))
    if(allocated(K)) deallocate(K)
    allocate(K(N))
  end subroutine

  subroutine step_helmholtz()
    double precision, dimension(:,:), allocatable :: M,M_inv
    double precision, dimension(:), allocatable :: K

    M = -matmul(matmul(P,A_inv),B) + Q
    M(1,:) = 0.0d0
    M(1,1) = 1.1d0
    M(size(M,1),:) = 0.0d0
    M(size(M,1),size(M,1)) = 1.0d0
    call print_matrix(Q,"log_Q.dat")
    call matr_invert(M,M_inv)
    K = R !- linapp(matmul(P,A_inv),bound)
    K(1) = bound(1)
    K(size(K)) = bound(size(K))

    phi = linapp(M_inv,K)

    !call system_solve(M,K,phi)
  end subroutine

  subroutine step_FTCS(dt,D)
    double precision :: dt,D
    integer :: i
    do i=2,size(phi)-1
      if(dx**2/(2*D) .lt. dt) write(*,*) "COURANT CONDITION VIOLATED"
      phi(i) = R(i) + dt*D/dx**2*(R(i+1) -2*R(i) + R(i-1))
    end do
    phi(1) = R(1)
    phi(size(phi)) = R(size(R))
  end subroutine

end module




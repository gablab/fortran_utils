! modMatrices.f90
! interface for some common use matrix utilities, depending on Lapack
! Gabriele Labanca 2019 gabrielelabanca@gmail.com
!
! system_solve: provide A,B,x to find x for the system Ax=B where A is a square matrix and x,B are vectors
!   - optional error: an estimate of the error is put in this parameter
!   - optional max_error: print a message if estimate of the error exceeds this parameter
!
! matr_invert: provide A,Ainv to invert A to Ainv where both are matrices 
! (copied from FortranWiki: http://fortranwiki.org/fortran/show/Matrix+inversion)


module modMatrices
  implicit none
contains
  subroutine system_solve(A,B,x,error,max_error) ! Ax=B
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(:), intent(in) :: B
    double precision, dimension(:), allocatable, intent(inout) :: x
    double precision, dimension(:), allocatable :: x_err
    double precision, dimension(:,:), allocatable :: A_work, B_work
    integer, dimension(:), allocatable :: IPIV
    integer :: N, NRHS, LDA, LDB, info
    double precision :: rms
    double precision, optional :: error,max_error

    N = size(A(:,1))
    LDA = size(A(1,:))
    if(N.ne.LDA) then
      write(*,*) "size of A: ",N,LDA
      stop "system_solve : The matrix to be inverted should be square!"
    end if
    NRHS = 1
    LDB = size(B)
    if(LDA.ne.LDB) then
      write(*,*) "size of A: ",N,LDA,"; size of B: ",NRHS,LDB
      stop "system_solve : Inconsistency of matrix dimensions!"
    end if

    ! prepare matrices to inversion
    call matr_copy(A,A_work)
    allocate(B_work(LDB,1))
    B_work(:,1) = B
    allocate(IPIV(N))

    ! call Lapack
    call DGESV(N, NRHS, A_work, LDA, IPIV, B_work, LDB, info)
    if(info .EQ. 0) then
      ! copy result to x
      if(allocated(x)) deallocate(x)
      allocate(x(LDB))
      x = B_work(:,1)

      ! check result
      x_err = linapp(A,x)-B
      x_err = x_err**2
      rms = sqrt(sum(x_err))/LDB
      if(present(max_error)) then
        if(rms.gt.max_error) then
          write(*,*) "system_solve : global error = ",rms,", bigger than threshold"
        end if
      end if
      if(present(error)) error = rms
    else
      call print_matrix(A,"log_syso_A.dat")
      open(1394,file='log_syso_B.dat')
      write(1394,*)B
      close(1394)
      stop "system_solve : ERROR during DGESV ("//char(info)//"); see log_syso_A.dat and log_syso_B.dat"
    end if
  end subroutine

  subroutine matr_invert(A,Ainv)
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(:,:), allocatable, intent(inout) :: Ainv

    if(allocated(Ainv)) deallocate(Ainv)
    allocate(Ainv(size(A,1),size(A,2)))
    Ainv = inv(A)
  end subroutine

  ! Returns the inverse of a matrix calculated by finding the LU
  ! decomposition.  Depends on LAPACK.
  ! Nota Bene: Some changes, e.g. not real(dp) but double precision
  function inv(A) result(Ainv)
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(size(A,1),size(A,2)) :: Ainv

    double precision, dimension(size(A,1)) :: work  ! work array for LAPACK
    integer, dimension(size(A,1)) :: ipiv   ! pivot indices
    integer :: n, info

    ! External procedures defined in LAPACK
    external DGETRF
    external DGETRI

    ! Store A in Ainv to prevent it from being overwritten by LAPACK
    Ainv = A
    n = size(A,1)

    ! DGETRF computes an LU factorization of a general M-by-N matrix A
    ! using partial pivoting with row interchanges.
    call DGETRF(n, n, Ainv, n, ipiv, info)

    if (info /= 0) then
      call print_matrix(A,"log_singular.dat")
      stop 'Matrix is numerically singular! (see log_singular.dat)'
    end if

    ! DGETRI computes the inverse of a matrix using the LU factorization
    ! computed by DGETRF.
    call DGETRI(n, Ainv, n, ipiv, work, n, info)

    if (info /= 0) then
      stop 'Matrix inversion failed!'
    end if
  end function inv

  subroutine print_vector(V,filename)
    double precision, dimension (:) :: V
    character (len=*), optional :: filename
    integer :: i

    if(present(filename)) then
      open(66,file=trim(filename))
    end if
    do i=1,size(V)
      if(present(filename)) then
        write(66,*) V(i)
      else
        write(*,*) V(i)
      end if 
    end do
    if(present(filename)) then
      close(66)
    end if
  end subroutine

  subroutine print_matrix(M,filename)
    double precision, dimension (:,:) :: M
    character (len=*), optional :: filename
    integer :: i

    if(present(filename)) then
      open(66,file=trim(filename))
    end if
    do i=1,size(M(:,1))
      if(present(filename)) then
        write(66,*) M(i,:)
      else
        write(*,*) M(i,:)
      end if 
    end do
    if(present(filename)) then
      close(66)
    end if
  end subroutine

  subroutine matr_casual(M,n)
    double precision, dimension (:,:), allocatable, intent(inout) :: M
    integer, intent(in) :: n
    double precision, dimension (:), allocatable :: arr
    integer :: i

    if(allocated(M)) deallocate(M)
    allocate(M(n,n))
    allocate(arr(n**2))

    do i=1,n*n
      arr(i) = atanh(RAND())
    end do

    M = transpose(reshape(arr,[n,n]))
    deallocate(arr)
  end subroutine 

  subroutine matr_copy(A,B)
    double precision, dimension (:,:), intent(in) :: A
    double precision, dimension (:,:), allocatable :: B
    integer :: m,n,i,j

    m = size(A(:,1))
    n = size(A(1,:))

    if(allocated(B)) deallocate(B)
    allocate(B(m,n))

    do i=1,m
      do j=1,n
        B(i,j) = A(i,j)
      end do
    end do
  end subroutine 

  function linapp(A,x) result(y)
    double precision, dimension(:,:), intent(in) :: A
    double precision, dimension(:), intent(in) :: x
    double precision, dimension(:), allocatable :: y
    integer :: m,n,i
    character(90) :: errmsg
    character(10) :: errx, errn

    m = size(A(:,1))
    n = size(A(1,:))
    if(n.ne.size(x)) then
      write(errx,'(I6.2)') size(x)
      write(errn,'(I6.2)') n
      errmsg = "linapp : Dimension of x ("//errx//&
        ") not consistent with "//errn//"!"
      stop errmsg
    end if
    allocate(y(n))

    do i=1,n
      y(i) = dot_product(A(i,:),x)
    end do

  end function

end module

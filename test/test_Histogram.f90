program main
  use modHistogram
  implicit none
  integer :: i,j
  double precision, dimension(10) :: array

  type(Histogram) :: h, hnorm, herr

  call h%init(nx=10,xmax=dble(3.14),xmin=dble(0.0),&
    nz=6,zmax=dble(1),zmin=dble(0.0))

  do i=1,10
    do j=1,10
      call h%fill(x=dble((i-1)*h%xmax/10),z=dble((j-1)*h%zmax/10))
    end do 
  end do

  write(*,*) h%nput,"entries"
  call h%prnt(additional_name="thisis1",additional_value=1.0d0)
  call h%dstr

  ! test norm
  write(*,*) "## Testing norm"
  call h%init(nx=5,xmax=dble(10.),xmin=dble(0.0),&
    ny=5,ymax=dble(10.),ymin=dble(0.0))
  call hnorm%init(nx=5,xmax=dble(10.),xmin=dble(0.0),&
    ny=5,ymax=dble(10.),ymin=dble(0.0))
  do i=1,10
    do j=1,10
      call h%fill(x=dble((i-1)*h%xmax/10),y=dble((j-1)*h%ymax/10))
      call hnorm%fill(x=3.0d0+0.5d0*i,y=3.0d0+0.5d0*i)
    end do
  end do
  write(*,*) "Initial histogram with norm"
  call h%prnt(column_name="value",additional_name="norm",additional_array=2.0d0*hnorm%hist)
  call h%norm(2.0d0)
  call h%norm(norm_histogram=hnorm)
  write(*,*) "Normed histogram"
  call h%prnt

  ! test sum
  write(*,*) "## Testing sum"
  call h%sumh(hnorm)
  write(*,*) "normed histogram + norm histogram"
  call h%prnt

  ! test custom binning
  write(*,*) "## Testing custom binning"
  array = (/ 1.4,1.6,1.7,1.3,0.7,0.8,1.123,0.1,0.23,0.65 /)
  call h%dstr
  call h%init(nx=10,xmax=dble(2.0),xmin=dble(0.0),&
    custom_bins=(/1.5d0,1.3d0,0.7d0,0.2d0,1.1d0,2.0d0,0d0/))
  do i=1,size(array)
    call h%fill(x=array(i))
  end do
  call h%prnt


  ! test errors
  call herr%init(nx=10,xmin=0.0d0,xmax=1.0d0,hname="hist_error")
  write(*,*) "Now the program should give an error"
  call herr%fill(x=2.0d0)


end program main

program main
  use modFiniteDifferences
  use modStats
  use modFiles
  implicit none
  integer, parameter :: N0 = 50
  integer, parameter :: Ntime = 1200
  integer :: i,time,ni,N
  double precision, parameter :: dt = 0.001d0, D = 1.0d0
  double precision, dimension(:), allocatable :: d2phi, d2analytical, analytical, arr_bound
  double precision :: phi_0, phi_L, xi, gam, t1, t2
  double precision, dimension(:,:), allocatable :: grid 

  N = N0
  call make_grid(grid,0.0d0,1.0d0,1.0d-1,N)


  write(*,*) "Test derivative: sin -> -sin"
  if(allocated(d2phi)) deallocate(d2phi)
  allocate(d2phi(N))
  if(allocated(analytical)) deallocate(analytical)
  allocate(analytical(N))
  if(allocated(phi)) deallocate(phi)
  allocate(phi(N))
  do i=1,N
    phi(i) = sin(dble(grid(i,1))*6.28)
  end do
  call init_finite_differences_helmholtz(N,grid(:,2),.0d0,.0d0,.0d0,.0d0,.0d0,.0d0)
  open(66,file='sin.dat')
  write(66,*) "position phi d2phi spacing"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) grid(i,1),phi(i),d2phi(i),grid(i,2)
  end do
  close(66)
  
  write(*,*) "Test derivative: cos -> -cos"
  if(allocated(d2phi)) deallocate(d2phi)
  allocate(d2phi(N))
  if(allocated(analytical)) deallocate(analytical)
  allocate(analytical(N))
  if(allocated(phi)) deallocate(phi)
  allocate(phi(N))
  do i=1,N
    phi(i) = cos(dble(grid(i,1))*6.28)
  end do
  call init_finite_differences_helmholtz(N,grid(:,2),.0d0,.0d0,.0d0,.0d0,.0d0,.0d0)
  open(66,file='cos.dat')
  write(66,*) "position phi d2phi spacing"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) grid(i,1),phi(i),d2phi(i),grid(i,2)
  end do
  close(66)

  write(*,*) "Test derivative: tan -> 2*tan(x)*(1+tan^2(x))"
  if(allocated(d2phi)) deallocate(d2phi)
  allocate(d2phi(N))
  if(allocated(analytical)) deallocate(analytical)
  allocate(analytical(N))
  if(allocated(phi)) deallocate(phi)
  allocate(phi(N))
  call init_finite_differences_helmholtz(N,grid(:,2),.0d0,.0d0,.0d0,.0d0,.0d0,.0d0)
  do i=1,N
    phi(i) = tan(grid(i,1))
  end do
  open(66,file='tan.dat')
  write(66,*) "position phi d2phi spacing"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) grid(i,1),phi(i),d2phi(i),grid(i,2)
  end do
  close(66)

end program

program main
  use modFiniteDifferences
  use modStats
  use modFiles
  implicit none
  integer, parameter :: N0 = 100
  integer, parameter :: Ntime = 1200
  integer :: i,time,ni,N
  double precision, parameter :: dt = 0.001d0, D = 1.0d0
  double precision, dimension(:), allocatable :: d2phi, d2analytical, analytical, arr_bound
  double precision :: phi_0, phi_L, xi, gam, t1, t2
  integer, dimension(4) :: N_array
  double precision, dimension(:), allocatable :: delta 
  double precision, dimension(:,:), allocatable :: grid 


  N_array(1) = N0/4
  N_array(2) = N0/2
  N_array(3) = N0
  N_array(4) = N0*2
  !N_array(5) = N0*4
  !N_array(6) = N0*8
  !N_array(7) = N0*16


  !-------------!
  !- DIFFUSION -!
  !-------------!
  write(*,*) "Solving diffusion"
  phi_0 = 1
  phi_L = 2



  call open_newfile(66,'diffusion.dat')
  write(66,*) "n_grid time position phi d2phi analytical"
  close(66)
  call open_newfile(67,'diffusion_stat.dat')
  write(67,*) "n_grid time error cpu_time"
  close(67)

  do ni=1,size(N_array)
    N = N_array(ni)
  call make_grid(grid,0.0d0,1.0d0,1.0d-10,N)
    !N = N0
    write(*,*) N,"grid points"
    if(allocated(d2phi)) deallocate(d2phi)
    allocate(d2phi(N))
    if(allocated(analytical)) deallocate(analytical)
    allocate(analytical(N))
    if(allocated(phi)) deallocate(phi)
    allocate(phi(N))

    if(allocated(arr_bound)) deallocate(arr_bound)
    allocate(arr_bound(N))
    arr_bound = 0.0d0
    arr_bound(1) = phi_0
    arr_bound(N) = phi_L

    allocate(delta(N))
    delta = 1.0d0/N
    call init_finite_differences_helmholtz(N,h_in=delta,&
      p1=1.0d0,q1=.0d0,r1=phi_0,&
      p2=1.0d0,q2=.0d0,r2=phi_L)
    deallocate(delta)
    ! fill matrices for Helmholtz equation
    P = 0.0d0
    Q = 0.0d0

    do i=1,N
      P(i,i) = -dt*D
      Q(i,i) = 1.0d0
      ! initial condition for phi
      phi(i) = sin(dble(i)/N*6.28) + sin(3*dble(i)/N*6.28)/2. &
        + sin(7*dble(i)/N*6.28)/3.
      analytical(i) = dble(i-1)/N * (phi_L - phi_0) + phi_0
    end do
    R = phi

    ! print for time 0
    d2phi = linapp(A_inv,(linapp(B,phi)))
    open(unit=66,file='diffusion.dat',status='old',position='append')
    do i=1,N
      write(66,*) N,0.0,grid(i,1),phi(i),d2phi(i),analytical(i)
    end do
    close(66)
    do time=1,Ntime
      ! print progress
      if(mod(time,int(0.1*Ntime)) .lt. 1.e-3) print*,100*float(time)/Ntime,' %'
      call cpu_time(t1)
      call step_helmholtz
      call cpu_time(t2)
      ! compute 2nd derivative
      d2phi = linapp(A_inv,(linapp(B,phi)))
      ! print results
      open(unit=66,file='diffusion.dat',status='old',position='append')
      do i=1,N
        write(66,*) N,time,grid(i,1),phi(i),d2phi(i),analytical(i)
      end do
      close(66)

      open(unit=67,file='diffusion_stat.dat',status='old',position='append')
      write(67,*) N,time,mean_root_difference(phi,analytical),t2-t1
      close(67)
      R = phi
    end do
  end do


end program

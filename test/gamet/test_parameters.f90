program main
  use modFiniteDifferences
  use modStats
  use modFiles
  implicit none
  integer, parameter :: N0 = 50
  integer :: i,time,ni,N
  double precision, parameter :: dt = 0.001d0, D = 1.0d0
  double precision, dimension(:), allocatable :: d2phi, d2analytical, analytical, arr_bound
  !double precision :: phi_0, phi_L, xi, gam, t1, t2
  double precision, dimension(:,:), allocatable :: grid 
  integer :: i1,i2,i3
  integer, parameter :: nparams = 10
  double precision :: extreme_param = -100.0d9
  double precision :: a1,a2,b2 ! parameters for Gamet1999


  N = N0
  call make_grid(grid,0.0d0,1.0d0,1.0d-1,N)

  call open_newfile(66,'tan.dat')
  write(66,*) "position phi d2phi spacing alpha1 alpha2 beta2"
  call open_newfile(67,'parameters_space.dat')
  write(67,*) "alpha1 alpha2 beta2 error"
  !do i1=1,nparams
   ! do i2=1,nparams
    !  do i3=1,nparams
        a1 = 11.0d0!+(-1.0d0+2.0d0*i1/dble(nparams)) !-extreme_param + dble(i1)*2*extreme_param/nparams
        a2 = 1.0d-1!+(-1.0d0+2.0d0*i2/dble(nparams)) ! -extreme_param + dble(i2)*2*extreme_param/nparams
        b2 = 1.0d-1!+(-1.0d0+2.0d0*i3/dble(nparams)) !-extreme_param + dble(i3)*2*extreme_param/nparams

        call set_gamet_boundaries(a1,a2,b2)

        write(*,*) "Test derivative: tan -> 2*tan(x)*(1+tan^2(x))"
        if(allocated(d2phi)) deallocate(d2phi)
        allocate(d2phi(N))
        if(allocated(analytical)) deallocate(analytical)
        allocate(analytical(N))
        if(allocated(phi)) deallocate(phi)
        allocate(phi(N))
        call init_finite_differences_helmholtz(N,grid(:,2),.0d0,.0d0,.0d0,.0d0,.0d0,.0d0)
        do i=1,N
          phi(i) = tan(grid(i,1))
          analytical = 2.d0*tan(grid(i,1))*(1+tan(grid(i,1))**2)
        end do
        d2phi = linapp(A_inv,(linapp(B,phi)))
        do i=1,N
          write(66,*) grid(i,1),phi(i),d2phi(i),grid(i,2),a1,a2,b2
        end do

        write(67,*) a1,a2,b2,mean_root_difference(d2phi,analytical)!/sum(analytical)*dble(size(analytical))

    !  end do 
   ! end do
  !end do
  close(66)
  close(67)

end program

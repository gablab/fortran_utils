program main
  use modFiniteDifferences
  use modStats
  use modFiles
  implicit none
  integer, parameter :: N0 = 50
  integer, parameter :: Ntime = 1200
  integer :: i,j,time,ni,N
  double precision, parameter :: dt = 0.001d0, D = 1.0d0
  double precision, dimension(:), allocatable :: d2phi, d2analytical, analytical, arr_bound
  double precision :: phi_0, phi_L, xi, gam, t1, t2, alpha
  integer :: n_alpha = 100
  double precision, dimension(:,:), allocatable :: grid 
  double precision, dimension(:,:), allocatable :: Atmp, Matr
  integer :: n_ignore_columns = 40
  integer :: Neig

  CHARACTER :: JOBVL, JOBVR
  INTEGER :: INFO, LDA, LDVL, LDVR, LWORK
  DOUBLE PRECISION, allocatable :: VL(:,:), VR(:,:), WORK(:), WR(:), WI(:)


  write(*,*) "Check that the solver works: eigenvalues should be 1,1,10"
  N = 3
  allocate(Atmp(N,N))
  Atmp(1,1) = 5
  Atmp(1,2) = 4 
  Atmp(1,3) = 2
  Atmp(2,1) = 4
  Atmp(2,2) = 5
  Atmp(2,3) = 2
  Atmp(3,1) = 2
  Atmp(3,2) = 2
  Atmp(3,3) = 2
  Neig = N 
  JOBVL = 'N'
  JOBVR = 'N'
  LDA = Neig
  if(LDA .ne. size(Atmp,1)) stop "size mismatch: LDA, Atmp"
  allocate(WR(Neig))
  allocate(WI(Neig))
  LDVL = 1
  LDVR = 1 
  allocate(VL(LDVL,1))
  allocate(VR(LDVR,1))
  LWORK = 4*(Neig)+1
  allocate(WORK(LWORK))
  call DGEEV(JOBVL,JOBVR,Neig,Atmp,LDA,WR,WI,VL,LDVL,VR,LDVR,WORK,LWORK,INFO)
  write(*,*) "Actual eigenvalues: "
  do i=1,N
    write(*,*) i,WR(i),WI(i)
  end do
  deallocate(WR,Wi,VL,VR,WORK)




  write(*,*) "Eigenvalues (Re):"
  do j=1,n_alpha
    alpha = 1.0d0*j/n_alpha


    N = N0
    call make_grid(grid,0.0d0,1.0d0,alpha,N)

    call init_finite_differences_helmholtz(N,grid(:,2),.0d0,.0d0,.0d0,.0d0,.0d0,.0d0)

    allocate(Matr(N,N))
    Matr = matmul(A_inv,B)



    ! https://www.math.utah.edu/software/lapack/lapack-d/dgeev.html
    call matr_copy(Matr(1+n_ignore_columns:,1+n_ignore_columns:),Atmp)
    Neig = N - n_ignore_columns
    JOBVL = 'N'
    JOBVR = 'N'
    LDA = Neig
    if(LDA .ne. size(Atmp,1)) stop "size mismatch: LDA, Atmp"
    allocate(WR(Neig))
    allocate(WI(Neig))
    LDVL = 1
    LDVR = 1 
    allocate(VL(LDVL,1))
    allocate(VR(LDVR,1))
    LWORK = 4*(Neig)+1
    allocate(WORK(LWORK))
    call DGEEV(JOBVL,JOBVR,Neig,Atmp,LDA,WR,WI,VL,LDVL,VR,LDVR,WORK,LWORK,INFO)
    !write(*,*) "After DGEEV, info = ",info

    call Bubble_Sort(WR)
    !call Bubble_Sort(WI)
    write(*,*) alpha,WR(Neig)!, WI(Neig)
    deallocate(Matr)
    deallocate(WR,Wi,VL,VR,WORK)
  end do

contains 

  ! from rosettacode.org/wiki/Sorting_algorithms/Bubble_sort#Fortran
  SUBROUTINE Bubble_Sort(a)
    double precision, INTENT(in out), DIMENSION(:) :: a
    double precision :: temp
    INTEGER :: i, j
    LOGICAL :: swapped

    DO j = SIZE(a)-1, 1, -1
      swapped = .FALSE.
      DO i = 1, j
        IF (a(i) > a(i+1)) THEN
          temp = a(i)
          a(i) = a(i+1)
          a(i+1) = temp
          swapped = .TRUE.
        END IF
      END DO
      IF (.NOT. swapped) EXIT
    END DO
  END SUBROUTINE Bubble_Sort

end program

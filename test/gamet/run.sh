rm -f test_Lele.exe

#https://stackoverflow.com/a/8878810/5599687

#case $1 in
  #'eigen')
    #name='test_eigen'
    ##gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_eigen.f90 -o test_eigen.exe
    ##./test_eigen.exe
    #;;
  #'derivative')
    #name='test_derivative'
    #gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_derivative.f90 -o test_derivative.exe
    #./test_derivative.exe
    #;;
  #'iterative')
    #gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_iterative.f90 -o test_iterative.exe
    #./test_iterative.exe
    #;;
  #'dirichlet')
    #gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_dirichlet.f90 -o test_dirichlet.exe
    #./test_dirichlet.exe
    #;;
  #'neumann')
    #gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_neumann.f90 -o test_neumann.exe
    #./test_neumann.exe
    #;;
  #'robin')
    #gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_robin.f90 -o test_robin.exe
    #./test_robin.exe
    #;;
  #'grid')
    #gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_grid.f90 -o test_grid.exe
    #./test_grid.exe
    #;;
  #*)
    #echo "no match!"
    #;;
#esac

rm -f test_$1.exe
gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Gamet1999.f90 ../../modFiniteDifferences.f90 test_$1.f90 -o test_$1.exe
./test_$1.exe



#gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Shukla2007.f90 ../../modFiniteDifferences.f90 test_Lele.f90 -o test_Lele.exe

rm -f *.mod
rm -f *.o

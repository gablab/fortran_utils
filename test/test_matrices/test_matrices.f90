program main
  use modMatrices
  implicit none

  double precision, dimension(:,:), allocatable :: A,A_inv,A_inv_inv
  double precision, dimension(:), allocatable :: B,x
  integer :: i,j
  integer :: nbig

  ! https://doi.org/10.1137/1014099 example 1.4
  allocate(A(2,2))
  A = transpose(reshape([0.01,1.0,1.0,101.0],[2,2]))
  allocate(B(2))
  B = [0.1,11.0]

  write(*,*) "A"
  call print_matrix(A)
  write(*,*) "B", B

  call system_solve(A,B,x)

  write(*,*) "RESULT (should be -90,1)"
  write(*,*) x

  write(*,*) "check: Ax (should be equal to B)"
  write(*,*) linapp(A,x)

  write(*,*) 
  write(*,*) "Now the same, but inverting A"
  call matr_invert(A,A_inv)
  write(*,*) "RESULT (should be -90,1)"
  write(*,*) linapp(A_inv,B)

  nbig = 1000
  write(*,*) 
  write(*,*) "Now a bigger matrix, ",nbig,'x',nbig,"; file matr_diff.dat should be ~0"
  deallocate(A)
  allocate(A(nbig,nbig))
  call srand(234987)
  do i=1,nbig
    do j=1,nbig
      A(i,j) = rand()
    end do
  end do
  call matr_invert(A,A_inv)
  call matr_invert(A_inv,A_inv_inv)
  call print_matrix(A-A_inv_inv,'matr_diff.dat')
  write(*,*) "sum of errors is ",sum(A-A_inv_inv)


  write(*,*) "Null system Ax=0"
  deallocate(B)
  allocate(B(nbig))
  do i=1,nbig
    B(i) = 0.0d0
  end do
  call system_solve(A,B,x)
  write(*,*) "result"
  write(*,*) x
  
  write(*,*) "System with B = (1,1,1,...1,1,1)"
  deallocate(B)
  allocate(B(nbig))
  do i=1,nbig
    B(i) = 1.0d0
  end do
  call system_solve(A,B,x)



contains


end program


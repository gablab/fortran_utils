program main
  use modFiniteDifferences
  use modStats
  use modFiles
  implicit none
  integer, parameter :: N0 = 100
  integer, parameter :: Ntime = 1200
  integer :: i,time,ni,N
  double precision, parameter :: dt = 0.001d0, D = 1.0d0
  double precision, dimension(:), allocatable :: d2phi, d2analytical, analytical, arr_bound
  double precision :: phi_0, phi_L, xi, gam, t1, t2
  integer, dimension(7) :: N_array


  N_array(1) = N0/4
  N_array(2) = N0/2
  N_array(3) = N0
  N_array(4) = N0*2
  N_array(5) = N0*4
  N_array(6) = N0*8
  N_array(7) = N0*16

  write(*,*) "Test derivative: sin -> -sin"
  N = N0
  if(allocated(d2phi)) deallocate(d2phi)
  allocate(d2phi(N))
  if(allocated(analytical)) deallocate(analytical)
  allocate(analytical(N))
  if(allocated(phi)) deallocate(phi)
  allocate(phi(N))
  call init_finite_differences_helmholtz(N,h_in=dble(6.28/N))
  do i=1,N
    phi(i) = sin(dble(i)/N*6.28)
  end do
  open(66,file='sin.dat')
  write(66,*) "position phi d2phi"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    write(66,*) i*6.28/N,phi(i),d2phi(i)
  end do
  close(66)

  write(*,*) "Test derivative: tan -> 2*tan(x)*(1+tan^2(x))"
  N = N0
  if(allocated(d2phi)) deallocate(d2phi)
  allocate(d2phi(N))
  if(allocated(analytical)) deallocate(analytical)
  allocate(analytical(N))
  if(allocated(phi)) deallocate(phi)
  allocate(phi(N))
  call init_finite_differences_helmholtz(N,h_in=1.0d0/N)
  do i=1,N
    xi = dble(i-1)/N 
    phi(i) = tan(xi)
  end do
  open(66,file='tan.dat')
  write(66,*) "position phi d2phi"
  d2phi = linapp(A_inv,(linapp(B,phi)))
  do i=1,N
    xi = dble(i-1)/N 
    write(66,*) xi,phi(i),d2phi(i)
  end do
  close(66)


  !-------------!
  !- DIFFUSION -!
  !-------------!
  write(*,*) "Solving diffusion"
  phi_0 = 1
  phi_L = 2



  call open_newfile(66,'diffusion.dat')
  write(66,*) "n_grid time position phi d2phi analytical"
  close(66)
  call open_newfile(67,'diffusion_stat.dat')
  write(67,*) "n_grid time error cpu_time"
  close(67)

  !do ni=1,size(N_array)
  !N = N_array(ni)
  N = N0
  write(*,*) N,"grid points"
  if(allocated(d2phi)) deallocate(d2phi)
  allocate(d2phi(N))
  if(allocated(analytical)) deallocate(analytical)
  allocate(analytical(N))
  if(allocated(phi)) deallocate(phi)
  allocate(phi(N))

  if(allocated(arr_bound)) deallocate(arr_bound)
  allocate(arr_bound(N))
  arr_bound = 0.0d0
  arr_bound(1) = phi_0
  arr_bound(N) = phi_L

  call init_finite_differences_helmholtz(N,h_in=1.0d0/N,b_phi=arr_bound)
  ! fill matrices for Helmholtz equation
  P = 0.0d0
  Q = 0.0d0

  do i=1,N
    P(i,i) = -dt*D
    Q(i,i) = 1.0d0
    ! initial condition for phi
    phi(i) = sin(dble(i)/N*6.28) + sin(3*dble(i)/N*6.28)/2. &
      + sin(7*dble(i)/N*6.28)/3.
    analytical(i) = dble(i-1)/N * (phi_L - phi_0) + phi_0
  end do
  R = phi

  ! print for time 0
  d2phi = linapp(A_inv,(linapp(B,phi)))
    open(unit=66,file='diffusion.dat',status='old',position='append')
  do i=1,N
    write(66,*) N,0.0,float(i)/N,phi(i),d2phi(i),analytical(i)
  end do
  close(66)
  do time=1,Ntime
    ! print progress
    if(mod(time,int(0.1*Ntime)) .lt. 1.e-3) print*,100*float(time)/Ntime,' %'
    open(unit=66,file='diffusion.dat',status='old',position='append')
    call cpu_time(t1)
    call step_helmholtz
    call cpu_time(t2)
    ! compute 2nd derivative
    d2phi = linapp(A_inv,(linapp(B,phi)))
    ! print results
    do i=1,N
      write(66,*) N,time,float(i)/N,phi(i),d2phi(i),analytical(i)
    end do
    close(66)
    open(unit=67,file='diffusion_stat.dat',status='old',position='append')
    write(67,*) N,time,mean_root_difference(phi,analytical),t2-t1
    close(67)
    R = phi
    !call step_FTCS(dt,D)
  end do
  !end do


  !-------------!
  !- HELMHOLTZ -!
  !-------------!
  write(*,*) "Solving generic Helmholtz"
  call open_newfile(66,'helmholtz.dat')
  write(66,*) "n_grid position phi analytical gamd2phi gamd2analytical error"
  close(66)
  call open_newfile(67,'helmholtz_stat.dat')
  write(67,*) "n_grid error cpu_time"
  close(67)


  do ni=1,size(N_array)
    N = N_array(ni)
    write(*,*) N,"grid points"
    !N = N0
    if(allocated(d2phi)) deallocate(d2phi)
    allocate(d2phi(N))
    if(allocated(analytical)) deallocate(analytical)
    allocate(analytical(N))
    if(allocated(phi)) deallocate(phi)
    allocate(phi(N))
    phi_0 = -314.54d0
    phi_L = -34.39d0
    gam = 1.0d0

    if(allocated(arr_bound)) deallocate(arr_bound)
    allocate(arr_bound(N))
    arr_bound = 0.0d0
    arr_bound(1) = phi_0
    arr_bound(N) = phi_L

    call init_finite_differences_helmholtz(N,h_in=1.d0/N,b_phi=arr_bound)
    ! fill matrices for Helmholtz equation
    P = 0.0d0
    Q = 0.0d0
    phi = 0.0d0
    R = 0.0d0

    do i=1,N
      P(i,i) = 1.0d0
      Q(i,i) = gam
      R(i) = 0.0d0
      xi = dble(i-1)/N 
      analytical(i) = (phi_L-phi_0*cos(sqrt(gam)))/sin(sqrt(gam))*sin(sqrt(gam)*xi) + &
        phi_0*cos(sqrt(gam)*xi)
    end do

    call cpu_time(t1)
    call step_helmholtz
    call cpu_time(t2)
    d2phi = linapp(A_inv,(linapp(B,phi)))
    d2analytical = linapp(A_inv,(linapp(B,analytical)))
    open(unit=66,file='helmholtz.dat',status='old',position='append')
    do i=1,N
      !d2analytical(i) = -(phi_L-phi_0*cos(sqrt(gam)))/sin(sqrt(gam))*sin(sqrt(gam)*xi) - &
      ! phi_0*cos(sqrt(gam)*xi)
      write(66,*) N,float(i)/N,phi(i),analytical(i),gam*d2phi(i),gam*d2analytical(i)
    end do
    close(66)
    open(unit=67,file='helmholtz_stat.dat',status='old',position='append')
      write(67,*) N,mean_root_difference(phi,analytical),t2-t1
      close(67)
  end do


  !--------------!
  !- ELLIPTICAL -!
  !--------------!
  write(*,*) "Solving elliptical"
  call open_newfile(66,'poisson.dat')
  write(66,*) "n_grid position phi analytical"
  close(66)
  call open_newfile(67,'poisson_stat.dat')
  write(67,*) "n_grid error cpu_time"
  close(67)

  do ni=1,size(N_array)
    N = N_array(ni)
    write(*,*) N,"grid points"
    !N = N0
    if(allocated(d2phi)) deallocate(d2phi)
    allocate(d2phi(N))
    if(allocated(analytical)) deallocate(analytical)
    allocate(analytical(N))
    if(allocated(phi)) deallocate(phi)
    allocate(phi(N))
    phi_0 = 0.1d0
    phi_L = 1.0d0

    if(allocated(arr_bound)) deallocate(arr_bound)
    allocate(arr_bound(N))
    arr_bound = 0.0d0
    arr_bound(1) = phi_0
    arr_bound(N) = phi_L
    phi = 0.0d0

    call init_finite_differences_helmholtz(N,h_in=1.d0/N,b_phi=arr_bound)
    ! fill matrices for Helmholtz equation
    P = 0.0d0
    Q = 0.0d0
    !phi = 0.0d0
    R = 0.0d0

    do i=1,N
      P(i,i) = 1.0d0
      xi = dble(i-1)/N 
      analytical(i) = int2a_pois(xi) + (phi_L-phi_0)/1. * xi - int2a_pois(1.d0)/1. * xi + phi_0
      R(i) = a_pois(xi)
    end do

    call cpu_time(t1)
    call step_helmholtz
    call cpu_time(t2)
    !d2phi = linapp(A_inv,(linapp(B,phi)))
    open(unit=66,file='poisson.dat',status='old',position='append')
    do i=1,N
      write(66,*) N,float(i)/N,phi(i),analytical(i)
    end do
    close(66)
    open(unit=67,file='poisson_stat.dat',status='old',position='append')
    write(67,*) N,mean_root_difference(phi,analytical),t2-t1
    close(67)
  end do


contains

  ! PAY ATTENTION: a(x) and int2a(x) are coupled: modify them both accordingly!
  function a_pois(x) result(y)
    double precision, intent(in) :: x
    double precision :: y
    !y = 0.0d0
    !y = 1.0d0
    !y = x
    y = sin(x)
  end function
  function int2a_pois(x) result(y)
    double precision, intent(in) :: x
    double precision :: y
    !y = 0.0d0
    !y = x**2/2.
    !y = x**3/6.
    y = -sin(x)
  end function

end program

program main
  use modFiniteDifferences
  use modStats
  use modFiles
  implicit none
  integer, parameter :: N0 = 100
  integer, parameter :: Ntime = 1200
  integer :: i,time,ni,N
  double precision, parameter :: dt = 0.001d0, D = 1.0d0
  double precision, dimension(:), allocatable :: d2phi, d2analytical, analytical, arr_bound
  double precision :: dphi_0, dphi_L, phi_0, phi_L, xi, gam, t1, t2
  integer, dimension(7) :: N_array
  double precision :: delta 


  N_array(1) = N0/4
  N_array(2) = N0/2
  N_array(3) = N0
  N_array(4) = N0*2
  N_array(5) = N0*4
  N_array(6) = N0*8
  N_array(7) = N0*16

  !-----------------------!
  !- HARMONIC OSCILLATOR -!
  !-----------------------!
  write(*,*) "Solving harmonic oscillator"
  call open_newfile(66,'harmonic.dat')
  write(66,*) "n_grid position phi analytical gamd2phi gamd2analytical"
  close(66)
  call open_newfile(67,'harmonic_stat.dat')
  write(67,*) "n_grid error cpu_time"
  close(67)


  do ni=1,size(N_array)
    N = N_array(ni)
    write(*,*) N,"grid points"
    !N = N0
    if(allocated(d2phi)) deallocate(d2phi)
    allocate(d2phi(N))
    if(allocated(analytical)) deallocate(analytical)
    allocate(analytical(N))
    if(allocated(phi)) deallocate(phi)
    allocate(phi(N))
    dphi_0 = -314.54d0
    dphi_L = -34.39d0
    gam = 1.0d0

    delta = 1.0d0/N
    call init_finite_differences_helmholtz(N,h_in=delta,&
      p1=0.0d0,q1=1.0d0,r1=dphi_0,&
      p2=0.0d0,q2=1.0d0,r2=dphi_L)
    ! fill matrices for harmonic oscillator equation
    P = 0.0d0
    Q = 0.0d0
    phi = 0.0d0
    R = 0.0d0

    do i=1,N
      P(i,i) = 1.0d0
      Q(i,i) = gam
      R(i) = 0.0d0
      xi = dble(i-1)/N 
      analytical(i) = (dphi_0*cos(sqrt(gam)) - dphi_L)/(sqrt(gam)*sin(sqrt(gam))) * cos(sqrt(gam)*xi) + &
        dphi_0/sqrt(gam) * sin(sqrt(gam)*xi)
    end do

    call cpu_time(t1)
    call step_helmholtz
    call cpu_time(t2)
    d2phi = linapp(A_inv,(linapp(B,phi)))
    d2analytical = linapp(A_inv,(linapp(B,analytical)))
    open(unit=66,file='harmonic.dat',status='old',position='append')
    do i=1,N
      write(66,*) N,float(i)/N,phi(i),analytical(i),gam*d2phi(i),gam*d2analytical(i)
    end do
    close(66)
    open(unit=67,file='harmonic_stat.dat',status='old',position='append')
    write(67,*) N,mean_root_difference(phi,analytical),t2-t1
    close(67)
  end do


  !--------------!
  !- ELLIPTICAL -!
  !--------------!
  write(*,*) "Solving Poisson"
  call open_newfile(66,'poisson.dat')
  write(66,*) "n_grid position phi analytical"
  close(66)
  call open_newfile(67,'poisson_stat.dat')
  write(67,*) "n_grid error cpu_time"
  close(67)

  do ni=1,size(N_array)
    N = N_array(ni)
    write(*,*) N,"grid points"
    !N = N0
    if(allocated(d2phi)) deallocate(d2phi)
    allocate(d2phi(N))
    if(allocated(analytical)) deallocate(analytical)
    allocate(analytical(N))
    if(allocated(phi)) deallocate(phi)
    allocate(phi(N))
    dphi_0 = 0.1d0
    dphi_L = inta_pois(1.0d0) + dphi_0

    
    if(allocated(arr_bound)) deallocate(arr_bound)
    allocate(arr_bound(N))
    arr_bound = 0.0d0
    arr_bound(1) = phi_0
    arr_bound(N) = phi_L
    phi = 0.0d0

    delta = 1.0d0/N
    call init_finite_differences_helmholtz(N,h_in=delta,&
      p1=0.0d0,q1=1.0d0,r1=dphi_0,&
      p2=0.0d0,q2=1.0d0,r2=dphi_L)
    ! fill matrices for Helmholtz equation
    P = 0.0d0
    Q = 0.0d0
    !phi = 0.0d0
    R = 0.0d0

    do i=1,N
      P(i,i) = 1.0d0
      xi = dble(i-1)/N 
      analytical(i) = int2a_pois(xi) + (phi_L-phi_0)/1. * xi - int2a_pois(1.d0)/1. * xi + phi_0
      R(i) = a_pois(xi)
    end do

    call cpu_time(t1)
    call step_helmholtz
    call cpu_time(t2)
    !d2phi = linapp(A_inv,(linapp(B,phi)))
    open(unit=66,file='poisson.dat',status='old',position='append')
    do i=1,N
      write(66,*) N,float(i)/N,phi(i),analytical(i)
    end do
    close(66)
    open(unit=67,file='poisson_stat.dat',status='old',position='append')
    write(67,*) N,mean_root_difference(phi,analytical),t2-t1
    close(67)
  end do


contains

  ! PAY ATTENTION: a(x), inta(x) and int2a(x) are coupled: modify them both accordingly!
  function a_pois(x) result(y)
    double precision, intent(in) :: x
    double precision :: y
    !y = 0.0d0
    !y = 1.0d0
    !y = x
    y = sin(x)
  end function
  function inta_pois(x) result(y)
    double precision, intent(in) :: x
    double precision :: y
    !y = 0.0d0
    !y = x
    !y = x**2/2.
    y = -cos(x)
  end function
  function int2a_pois(x) result(y)
    double precision, intent(in) :: x
    double precision :: y
    !y = 0.0d0
    !y = x**2/2.
    !y = x**3/6.
    y = -sin(x)
  end function

end program

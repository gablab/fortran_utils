rm -f test_Lele.exe

#https://stackoverflow.com/a/8878810/5599687

case $1 in
  'derivative')
    gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Lele1992.f90 ../../modFiniteDifferences.f90 test_derivative.f90 -o test_derivative.exe
    ./test_derivative.exe
    ;;
  'iterative')
    gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Lele1992.f90 ../../modFiniteDifferences.f90 test_iterative.f90 -o test_iterative.exe
    ./test_iterative.exe
    ;;
  'dirichlet')
    gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Lele1992.f90 ../../modFiniteDifferences.f90 test_dirichlet.f90 -o test_dirichlet.exe
    ./test_dirichlet.exe
    ;;
  'neumann')
    gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Lele1992.f90 ../../modFiniteDifferences.f90 test_neumann.f90 -o test_neumann.exe
    ./test_neumann.exe
    ;;
  'robin')
    gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Lele1992.f90 ../../modFiniteDifferences.f90 test_robin.f90 -o test_robin.exe
    ./test_robin.exe
    ;;
  *)
    echo "no match!"
    ;;
esac



#gfortran -llapack -cpp  ../../modFiles.f90 ../../modStats.f90 ../../modMatrices.f90 ../../Lele1992.f90 ../../modFiniteDifferences.f90 test_Lele.f90 -o test_Lele.exe

rm -f *.mod
rm -f *.o

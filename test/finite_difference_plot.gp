
set ylabel 'error'; set xlabel 'x'; pl 'gamet/sin_shukla_alpha1.dat' u 1:(error($3,-(6.28**2)*sin($1*6.28))) w l t 'Shukla, alpha = 1', 'gamet/sin_shukla_alpha0.9.dat' u 1:(error($3,-(6.28**2)*sin($1*6.28))) w l t 'Shukla, alpha = 0.9',  'gamet/sin_shukla_alpha0.dat' u 1:(error($3,-(6.28**2)*sin($1*6.28))) w l t 'Shukla, alpha \~ 0'

set ylabel 'error'; set xlabel 'x'; pl 'lele/sin.dat' u 1:(error($3,-(6.28**2)*sin($1*6.28))) w l t 'Lele', 'gamet/sin_shukla_alpha1.dat' u 1:(error($3,-(6.28**2)*sin($1*6.28))) w l t 'Shukla, alpha = 1'





file = 'lele/sin.dat'; set xlabel 'x'; set ylabel "f''"; pl file u 1:3 w p pt 7 lc 'blue' t 'numerical', '' u 1:(-(6.28**2)*sin($1*6.28)) w l lc 'red' t 'analytical'

program main
  use modFiniteDifferences

  implicit none

  call init_finite_differences(100)
  call print_matrix(A,"a.dat")
  call print_matrix(B,"b.dat")

end program

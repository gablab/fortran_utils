module modStats
  implicit none
contains

  function mean_root_difference(a,b) result(y)
    double precision, dimension(:), intent(in) :: a,b
    double precision :: y
    integer :: i
    y = 0.0d0
    if(size(a) .ne. size(b)) stop "mean_root_difference : array sizes don't match" 
    do i=1,size(a)
      y = y + (a(i) - b(i))**2
    end do
    y = sqrt(y / size(a))
  end function

end module

module modString
contains

  function format_string_separators(str) result(y)
    implicit none
    character(len=*), intent(in) :: str
    character(len=:), allocatable :: tmp,y
    integer :: ic,ih
    character :: targetch = ' '
    logical :: new_start, notyet_space

    allocate(character(len=len(str)) :: tmp)
    ih = 1
    new_start = .false.
    do ic=1,len(str)
      if (str(ic:ic) .ne. targetch) then
        tmp(ih:ih) = str(ic:ic)
        ih = ih + 1
        new_start = .true.
        notyet_space = .true.
      else if(new_start .and. notyet_space) then
        tmp(ih:ih) = targetch
        ih = ih + 1
        notyet_space = .false.
      end if
    end do
    tmp(ih+1:) = ' '
    allocate(character(len=len(trim(tmp))) :: y)
    y = tmp(1:len(y))
  end function format_string_separators

  function get_word(str,nw) result(word)
    implicit none
    character(len=*), intent(in) :: str
    integer, intent(in) :: nw
    character(len=:), allocatable :: tmp,word
    integer :: ic,iw,it
    character :: targetch = ' '
    logical :: new_start, notyet_space
    
    allocate(character(len=len(format_string_separators(str))) :: tmp)
    tmp = format_string_separators(str)
    iw = 0
    it = 1
    new_start = .false.
    do ic=1,len(str)
      if (tmp(ic:ic) .eq. targetch) then
        iw = iw + 1
        if (iw .eq. nw) then
          allocate(character(len=(ic-it+1)) :: word)
          word = tmp(it:ic)
          return
        end if
        it = ic
      end if
    end do
    !return error string
    allocate(character(len=(1)) :: word)
    word = '0'
    return

  end function get_word

end module modString

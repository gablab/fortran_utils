! modHistogram.f90
! class for histograms 
! Gabriele Labanca 2019
!
! SAMPLE USE:
! >  use modHistogram
! >  type(Histogram) :: h
! >  call h%init(nx=10,xmax=dble(3.14),xmin=dble(0.0),&
! >    ny=2,ymax=dble(1),ymin=dble(0.0))
! >  call h%fill(x=dble(1.3),y=dble(0.2))
! >  call h%prnt("myfile.dat")
!
! ADVANCED FEATURES:
!
! - (BETA) custom bins: provide custom array with bins extremes during initialization
!     [ ] not well integrated: _min, _max, n_ are unused parameters (=> inheritance?)
!     [ ] works only 1D (is it sensical to make it ND?)


module modHistogram
  implicit none
  private

  type, public :: Histogram
    double precision, dimension(:,:,:), allocatable :: hist
    double precision :: xmin,ymin,zmin,xmax,ymax,zmax,bx,by,bz
    integer          :: nx,ny,nz
    integer          :: ndim,nput
    character(len=:), allocatable :: hname
    double precision, dimension(:), allocatable :: custom_bins
    logical :: newfile = .true.
  contains
    procedure :: init => hist_init
    procedure :: geti => hist_getindex
    procedure :: fill => hist_fill
    procedure :: prnt => hist_print
    procedure :: getv => hist_getvalue
    procedure :: norm => hist_norm
    procedure :: dstr => hist_destroy
    procedure :: sumh => hist_sumhere
  end type Histogram
  public :: getindex_for_histogram
contains

  subroutine hist_init(this,nx,ny,nz,&
      xmin,ymin,zmin,xmax,ymax,zmax,hname,&
      custom_bins)
    implicit none
    class(Histogram), intent(inout) :: this
    integer, intent(in), optional :: nx,ny,nz
    double precision, intent(in), optional :: xmin,ymin,zmin,xmax,ymax,zmax
    character(len=*), optional :: hname
    double precision, intent(in), dimension(:), optional :: custom_bins

    this%nput = 0
    this%ndim = 0
    ! for each optional argument, check consistency (both n_ and l_ should be present)
    !  then assign to class variables 
    if(present(nx)) then
      if(.not.present(xmin)) write(*,*) "ERROR: Histogram%hist_init, xmin missing"
      if(.not.present(xmax)) write(*,*) "ERROR: Histogram%hist_init, xmax missing"
      this%nx = nx
      this%xmin = xmin
      this%xmax = xmax
      this%ndim = this%ndim + 1
    else
      this%nx = 1
      this%xmin = 0.0
      this%xmax = 0.0
    end if 
    if(present(ny)) then
      if(.not.present(ymin)) write(*,*) "ERROR: Histogram%hist_init, ymin missing"
      if(.not.present(ymax)) write(*,*) "ERROR: Histogram%hist_init, ymax missing"
      this%ny = ny
      this%ymin = ymin
      this%ymax = ymax
      this%ndim = this%ndim + 1
    else
      this%ny = 1
      this%ymin = 0.0
      this%ymax = 0.0
    end if 
    if(present(nz)) then
      if(.not.present(zmin)) write(*,*) "ERROR: Histogram%hist_init, zmin missing"
      if(.not.present(zmax)) write(*,*) "ERROR: Histogram%hist_init, zmax missing"
      this%nz = nz
      this%zmin = zmin
      this%zmax = zmax
      this%ndim = this%ndim + 1
    else
      this%nz = 1
      this%zmin = 0.0
      this%zmax = 0.0
    end if 

    this%bx = (this%xmax-this%xmin)/this%nx
    this%by = (this%ymax-this%ymin)/this%ny
    this%bz = (this%zmax-this%zmin)/this%nz

    if(present(hname)) then
      this%hname = hname(:)
    end if

    if(present(custom_bins)) then
      allocate(this%custom_bins(size(custom_bins)))
      this%custom_bins = custom_bins
      !call Bubble_Sort(this%custom_bins)
      allocate(this%hist(size(this%custom_bins)-1,1,1))
    else
      allocate(this%hist(this%nx,this%ny,this%nz))
    end if

    this%hist = dble(0.0)
  end subroutine

  subroutine hist_fill(this,x,y,z,w)
    class(Histogram), intent(inout) :: this
    double precision, intent(in), optional :: x,y,z,w
    integer :: ix,iy,iz
    double precision :: weight

    if(present(x)) then
      ix = this%geti(x,1)
    else
      ix = 1
    end if
    if(present(y)) then
      iy = this%geti(y,2)
    else
      iy = 1
    end if
    if(present(z)) then
      iz = this%geti(z,3)
    else 
      iz = 1
    end if
    if(present(w)) then 
      weight = w
    else 
      weight = dble(1.0)
    end if

    this%hist(ix,iy,iz) = this%hist(ix,iy,iz) + weight
    this%nput = this%nput + 1
  end subroutine

  function hist_getindex(this,x,direction) result(ix)
    class(Histogram), intent(inout) :: this
    double precision, intent(in) :: x
    integer, intent(in) :: direction
    integer :: ix
    if(allocated(this%custom_bins)) then
      ix = getindex_in_array(x,this%custom_bins)      
    else
      if(direction.eq.1) then 
        ix = getindex_for_histogram(x,this%xmin,this%xmax,this%nx)
      else if(direction.eq.2) then 
        ix = getindex_for_histogram(x,this%ymin,this%ymax,this%ny)
      else if(direction.eq.3) then
        ix = getindex_for_histogram(x,this%zmin,this%zmax,this%nz)
      else 
        write(*,*)"Histogram%geti: wrong or no direction provided"
        ix = -1
      end if
    end if

    if(ix.eq.-1 .and. allocated(this%hname)) then
      write(*,*) ">> error from ",this%hname
      stop
    end if
  end function

  function getindex_for_histogram(x,xmin,xmax,nx) result(idx)
    double precision, intent(in) :: x,xmin,xmax
    integer, intent(in) :: nx
    double precision :: bx
    integer :: idx
    ! check that the input is in boundaries, then find correspondent bin
    ! if a direction is not given, then select first (dummy) bin
    bx = (xmax-xmin)/nx
    if(x.gt.xmax) then
      write(*,*)"histogram : out of range; x = ",x,">",xmax
      idx = -1
      return
    else if(x.lt.xmin) then
      write(*,*)"histogram: out of range; x = ",x,"<",xmin
      idx = -1
      return
    else
      if(x.eq.xmax) then
        idx = nx
      else
        idx = floor((x-xmin)/bx)+1
      end if
    end if
  end function


  function getindex_in_array(x,bins) result(idx)
    double precision, intent(in) :: x
    double precision, dimension(:), optional :: bins ! optionally provide custom (i.e. nonlinear) binning
    integer :: idx
    integer :: i
    ! check that the input is in boundaries, then find correspondent bin
    ! if a direction is not given, then select first (dummy) bin
    if(x.gt.bins(size(bins))) then
      write(*,*)"histogram : out of range; x = ",x,">",bins(size(bins))
      idx = -1
      return
    else if(x.lt.bins(1)) then
      write(*,*)"histogram: out of range; x = ",x,"<",bins(1)
      idx = -1
      return
    else
      if(x.eq.bins(size(bins))) then
        idx = size(bins)
      else
        do i=1,size(bins)
          if( bins(i).lt.x .and. bins(i+1).gt.x ) then
            idx = i
            exit
          end if
        end do
      end if
    end if
  end function


  subroutine hist_print(this,filename,column_name,&
      additional_name,additional_value,additional_array,append) 
    class(Histogram), intent(inout) :: this
    character(*), optional :: filename, column_name, additional_name
    integer :: unit_out, i,j,k
    double precision, optional :: additional_value
    double precision, dimension(:,:,:), optional :: additional_array
    character(100) :: vars
    logical, optional :: append
    character(10) :: app

    app = "asis"
    if(present(append) .and. append) app = "append"

    if(present(filename)) then
      unit_out = 66
      open(unit=unit_out,file=trim(filename),position=trim(app))
    else
      unit_out = 6
    end if

    if(this%newfile) then
      vars = ""
      if(this%nx .gt. 1) vars = trim(vars)//" x "
      if(this%ny .gt. 1) vars = trim(vars)//" y "
      if(this%nz .gt. 1) vars = trim(vars)//" z "
      if(present(additional_name)) vars = trim(vars)//" "//trim(additional_name)
      if(present(column_name)) then
        vars = trim(vars)//" "//trim(column_name)
      else
        vars = trim(vars)//"  frequency"
      end if
      write(unit_out,*) trim(vars)
      write(unit_out,*)
      this%newfile = .false.
    end if

    if(allocated(this%custom_bins)) then
      do i=1,size(this%custom_bins)-1 
        write(unit_out,'(es12.5,2x)',advance='no') (this%custom_bins(i+1)+this%custom_bins(i))/2.0d0
        if(present(additional_value)) write(unit_out,'(es12.5,2x)',advance='no') additional_value
        if(present(additional_array)) write(unit_out,'(es12.5,2x)',advance='no') additional_array(i,1,1)
        write(unit_out,'(es12.5,2x)')  this%hist(i,1,1)
      end do
    else
      do i=1,this%nx
        do j=1,this%ny
          do k=1,this%nz
            if(this%nx .gt. 1) write(unit_out,'(es12.5,2x)',advance='no') this%xmin+i*this%bx
            if(this%ny .gt. 1) write(unit_out,'(es12.5,2x)',advance='no') this%ymin+j*this%by
            if(this%nz .gt. 1) write(unit_out,'(es12.5,2x)',advance='no') this%zmin+k*this%bz
            if(present(additional_value)) write(unit_out,'(es12.5,2x)',advance='no') additional_value
            if(present(additional_array)) write(unit_out,'(es12.5,2x)',advance='no') additional_array(i,j,k)
            write(unit_out,'(es12.5,2x)')  this%hist(i,j,k)
          end do
        end do 
      end do
    end if

  end subroutine

  function hist_getvalue(this,ix,iy,iz) result(val)
    class(Histogram), intent(in) :: this
    integer, intent(in), optional :: ix,iy,iz
    integer :: i,j,k
    double precision :: val
    if(present(ix)) then
      i = ix
    else
      i = 1
    end if
    if(present(iy)) then
      j = iy
    else
      j = 1
    end if
    if(present(iz)) then
      k = iz
    else
      k = 1
    end if
    val = this%hist(i,j,k)
  end function

  subroutine hist_norm(this,norm,norm_histogram,norm_array,binwidth)
    class(Histogram), intent(inout) :: this
    double precision, optional :: norm
    class(Histogram), optional :: norm_histogram
    double precision, dimension(:), optional :: norm_array
    character, optional :: binwidth
    integer :: i
    if(present(norm) .and. norm.ne.0.0d0) then
      this%hist = this%hist / norm
    end if
    if(present(norm_histogram)) then
      where(norm_histogram%hist .ne. 0.0d0) 
        this%hist = this%hist / norm_histogram%hist
      end where
    end if
    if(present(norm_array)) then
      where(norm_array .ne. 0.0d0) 
        this%hist(:,1,1) = this%hist(:,1,1) / norm_array
      end where
    end if
    if(present(binwidth)) then
      if(binwidth.eq.'y' .and. allocated(this%custom_bins)) then
        do i=1,size(this%hist(:,1,1)) 
          this%hist(i,1,1) = this%hist(i,1,1) / ( &
            this%custom_bins(i+1) - this%custom_bins(i) &
            )
        end do
      end if
    end if
  end subroutine


  subroutine hist_destroy(this)
    class(Histogram), intent(inout) :: this
    deallocate(this%hist)
    if(allocated(this%custom_bins)) deallocate(this%custom_bins)
    this%newfile = .true.
  end subroutine

  subroutine hist_sumhere(this,that)
    class(Histogram), intent(inout) :: this
    class(Histogram), intent(in) :: that
    this%hist = this%hist + that%hist
  end subroutine

  ! from rosettacode.org/wiki/Sorting_algorithms/Bubble_sort#Fortran
  !SUBROUTINE Bubble_Sort(a)
  !  double precision, INTENT(in out), DIMENSION(:) :: a
  !  double precision :: temp
  !  INTEGER :: i, j
  !  LOGICAL :: swapped
  !
  !    DO j = SIZE(a)-1, 1, -1
  !      swapped = .FALSE.
  !      DO i = 1, j
  !        IF (a(i) > a(i+1)) THEN
  !          temp = a(i)
  !          a(i) = a(i+1)
  !          a(i+1) = temp
  !          swapped = .TRUE.
  !        END IF
  !      END DO
  !      IF (.NOT. swapped) EXIT
  !    END DO
  !  END SUBROUTINE Bubble_Sort
end module modHistogram



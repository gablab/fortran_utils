module FD_method
  implicit none

  double precision, parameter :: alpha_param_6 = 1./10, beta_param_6 = 0, a_6 = 6./5, b_6 = 0, c_6 = 0, &
    alpha_param_10 =0.291772688719254, beta_param_10 = 0.00975402883799830, a_10 = 0.814249363867684, &
    b_10 = 0.788804071246819, c_10  = 0.0,&
    alpha_param_12 = 334./899, beta_param_12 = 43./1798, a_12 = 1065./1798, b_12 = 1038./899, c_12 = 79./1798,&
    alpha_param_B3 = 11.0d0, a_B3 = 13., b_B3 = -27.,&
    c_B3 = 15., d_B3 = -1.

contains

  subroutine init_finite_differences_method(A,B,N,h_vect)
    double precision, dimension(:,:), allocatable :: A,B
    integer, intent(in) :: N
    integer :: i
    double precision, intent(in), dimension(:) :: h_vect
    double precision :: h

    if(allocated(A)) deallocate(A)
    allocate(A(N,N))
    if(allocated(B)) deallocate(B)
    allocate(B(N,N))
    A = 0.0d0
    B = 0.0d0

    h = sum(h_vect)/dble(N)

    do i=1,N
      A(i,i) = 1.0d0
      if(i.gt.1 .and. i.lt.N) then
        B(i,i) = -2.0 * ( c_param(i,N)/(3*h)**2 &
          + b_param(i,N)/(2*h)**2 &
          + a_param(i,N)/(h)**2  &
          )  
        A(i,i-1) = alpha_param(i,N)
        A(i,i+1) = alpha_param(i,N)
        B(i,i-1) = a_param(i,N) / h**2
        B(i,i+1) = a_param(i,N) / h**2
        if(i.gt.2 .and. i.lt.(N-1)) then
          A(i,i-2) = beta_param(i,N)
          A(i,i+2) = beta_param(i,N)
          B(i,i-2) = b_param(i,N) / ( 2*h )**2
          B(i,i+2) = b_param(i,N) / ( 2*h )**2
          if(i.gt.3 .and. i.lt.(N-2)) then
            B(i,i-3) = c_param(i,N) / ( 3*h )**2
            B(i,i+3) = c_param(i,N) / ( 3*h )**2
          end if
        end if
      else if(i .eq. 1) then
        A(i,i+1) = alpha_param(i,N)
        B(i,i)   = a_param(i,N) / h**2
        B(i,i+1) = b_param(i,N) / h**2
        B(i,i+2) = c_param(i,N) / h**2
        B(i,i+3) = d_param(i,N) / h**2
      else if(i .eq. N) then
        A(i,i-1) = alpha_param(i,N)
        B(i,i)   = a_param(i,N) / h**2
        B(i,i-1) = b_param(i,N) / h**2
        B(i,i-2) = c_param(i,N) / h**2
        B(i,i-3) = d_param(i,N) / h**2
      else
        print*,i,"Lele1991.f90::init_finite_differences::WARNING out of matrix?"
      end if
    end do
    !B = B/(h**2)




  end subroutine

  function alpha_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    if(j.eq.1 .or. j.eq.N) then
      y = alpha_param_B3
    else if (j .eq. 2 .or. j.eq.(N-1)) then
      y= alpha_param_6
    else if( j .eq. 3 .or. j.eq.(N-2)) then
      y = alpha_param_10
    else
      y = alpha_param_12
    end if
  end function

  function beta_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    if(j .eq. 1 .or. j.eq.N) then
      y = 0.0d0
    else if(j .eq. 2 .or. j .eq.(N-1)) then
      y = beta_param_6
    else if(j .eq. 3 .or. j .eq.(N-2)) then
      y = beta_param_10
    else
      y = beta_param_12
    end if
  end function

  function a_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    if(j .eq. 1 .or. j.eq.N) then
      y = a_B3
    else if(j .eq. 2.or. j .eq.N-1) then
      y = a_6
    else if(j .eq. 3.or. j .eq.N-2) then
      y = a_10
    else
      y = a_12
    end if
  end function

  function b_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    if(j .eq. 1 .or. j.eq.N) then
      y = b_B3
    else if(j .eq. 2 .or. j .eq.N-1) then
      y = b_6
    else if(j .eq. 3 .or. j .eq.N-2) then
      y = b_10
    else
      y = b_12
    end if
  end function

  function c_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    if(j .eq. 1 .or. j.eq.N) then
      y = c_B3
    else if(j .eq. 2 .or. j .eq.N-1) then
      y = c_6
    else if(j .eq. 3 .or. j .eq.N-2) then
      y = c_10
    else
      y = c_12
    end if
  end function

  function d_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    if(j .eq. 1 .or. j.eq.N) then
      y = d_B3
    else 
      y = 0.0d0
    end if
  end function

  function k_param(j,N) result(y)
    integer, intent(in) :: j,N
    double precision :: y
    y = -2.0 * ( c_param(j,N)/9. + b_param(j,N)/4. + a_param(j,N) )  
  end function

  subroutine make_grid(grid,a,b,alpha,N)
    ! grid is an array (N,2) where (:,1) are the gridpoints, (:,2) are the dx
    double precision, dimension(:,:), allocatable :: grid
    integer, intent(in) :: N
    integer :: ng,ig
    double precision :: a,b,alpha
    ng = N 
    if(allocated(grid)) deallocate(grid)
    allocate(grid(N,2))
    do ig=1,ng
      grid(ig,1) = (b-a)/dble(ng)*ig + a
      grid(ig,2) = (b-a)/dble(ng) 
    end do
  end subroutine
end module

